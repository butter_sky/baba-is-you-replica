import java.awt.event.KeyEvent;

/**
 * 编写方法对所有关卡进行初始化
 */
public class Level {
    /**
     * 对所有关卡进行初始化
     *
     * @param level 关卡编号
     * @return Board 对应关卡
     */
    public static Board getLevel(int level) {
        if (level == 0) {
            Board b = new Board(25, 15, 50);
            for (int i = 8; i <= 18; i++)
                b.addSquare(new Square(i, 6, "item", "wall"));
            for (int i = 8; i <= 18; i++)
                b.addSquare(new Square(i, 10, "item", "wall"));
            b.addSquare(new Square(9, 8, "item", "baba"));
            b.addSquare(new Square(17, 8, "item", "flag"));
            b.addSquare(new Square(13, 7, "item", "rock"));
            b.addSquare(new Square(13, 8, "item", "rock"));
            b.addSquare(new Square(13, 9, "item", "rock"));
            b.addSquare(new Square(8, 4, "noun", "wall"));
            b.addSquare(new Square(9, 4, "operator", "is"));
            b.addSquare(new Square(10, 4, "property", "stop"));
            b.addSquare(new Square(16, 4, "noun", "rock"));
            b.addSquare(new Square(17, 4, "operator", "is"));
            b.addSquare(new Square(18, 4, "property", "push"));
            b.addSquare(new Square(8, 12, "noun", "baba"));
            b.addSquare(new Square(9, 12, "operator", "is"));
            b.addSquare(new Square(10, 12, "property", "you"));
            b.addSquare(new Square(16, 12, "noun", "flag"));
            b.addSquare(new Square(17, 12, "operator", "is"));
            b.addSquare(new Square(18, 12, "property", "win"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 1) {
            Board b = new Board(26, 17, 50);
            b.addSquare(new Square(14, 10, "item", "flag"));
            b.addSquare(new Square(17, 5, "item", "baba"));
            for (int i = 8; i <= 19; i++)
                b.addSquare(new Square(i, 8, "item", "wall"));
            for (int i = 12; i <= 19; i++)
                b.addSquare(new Square(i, 2, "item", "wall"));
            for (int i = 3; i <= 7; i++)
                b.addSquare(new Square(12, i, "item", "wall"));
            for (int i = 3; i <= 7; i++)
                b.addSquare(new Square(19, i, "item", "wall"));
            for (int i = 9; i <= 12; i++)
                b.addSquare(new Square(8, i, "item", "wall"));
            for (int i = 9; i <= 16; i++)
                b.addSquare(new Square(19, i, "item", "wall"));
            for (int i = 13; i <= 15; i++)
                b.addSquare(new Square(12, i, "item", "wall"));
            for (int i = 9; i <= 12; i++)
                b.addSquare(new Square(i, 12, "item", "wall"));
            for (int i = 12; i <= 18; i++)
                b.addSquare(new Square(i, 16, "item", "wall"));
            b.addSquare(new Square(9, 6, "noun", "baba"));
            b.addSquare(new Square(9, 5, "operator", "is"));
            b.addSquare(new Square(9, 4, "property", "you"));
            b.addSquare(new Square(14, 6, "noun", "wall"));
            b.addSquare(new Square(14, 5, "operator", "is"));
            b.addSquare(new Square(14, 4, "property", "stop"));
            b.addSquare(new Square(10, 10, "noun", "flag"));
            b.addSquare(new Square(14, 14, "operator", "is"));
            b.addSquare(new Square(17, 12, "property", "win"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 2) {
            Board b = new Board(26, 17, 50);
            for (int i = 8; i <= 19; i++)
                b.addSquare(new Square(i, 8, "item", "flag"));
            for (int i = 12; i <= 16; i++)
                b.addSquare(new Square(i, 2, "item", "flag"));
            for (int i = 16; i <= 19; i++)
                b.addSquare(new Square(i, 3, "item", "flag"));
            for (int i = 8; i <= 12; i++)
                b.addSquare(new Square(i, 12, "item", "flag"));
            for (int i = 12; i <= 19; i++)
                b.addSquare(new Square(i, 16, "item", "flag"));
            for (int i = 9; i <= 11; i++)
                b.addSquare(new Square(8, i, "item", "flag"));
            for (int i = 3; i <= 7; i++)
                b.addSquare(new Square(12, i, "item", "flag"));
            for (int i = 13; i <= 15; i++)
                b.addSquare(new Square(12, i, "item", "flag"));
            for (int i = 4; i <= 7; i++)
                b.addSquare(new Square(19, i, "item", "flag"));
            for (int i = 9; i <= 15; i++)
                b.addSquare(new Square(19, i, "item", "flag"));
            b.addSquare(new Square(17, 5, "item", "wall"));
            b.addSquare(new Square(9, 6, "noun", "wall"));
            b.addSquare(new Square(9, 5, "operator", "is"));
            b.addSquare(new Square(9, 4, "property", "you"));
            b.addSquare(new Square(14, 6, "noun", "flag"));
            b.addSquare(new Square(14, 5, "operator", "is"));
            b.addSquare(new Square(14, 4, "property", "stop"));
            b.addSquare(new Square(10, 10, "noun", "baba"));
            b.addSquare(new Square(14, 14, "operator", "is"));
            b.addSquare(new Square(17, 12, "property", "win"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 3) {
            Board b = new Board(22, 16, 50);
            b.addSquare(new Square(10, 13, "item", "baba"));
            b.addSquare(new Square(13, 13, "item", "rock"));
            b.addSquare(new Square(13, 11, "item", "rock"));
            b.addSquare(new Square(6, 3, "item", "flag"));
            b.addSquare(new Square(10, 5, "item", "wall"));
            b.addSquare(new Square(1, 13, "item", "wall"));
            b.addSquare(new Square(2, 13, "item", "wall"));
            for (int i = 13; i <= 16; i++)
                b.addSquare(new Square(3, i, "item", "wall"));
            for (int i = 5; i <= 18; i++)
                b.addSquare(new Square(i, 2, "item", "wall"));
            for (int i = 5; i <= 8; i++)
                b.addSquare(new Square(i, 9, "item", "wall"));
            for (int i = 12; i <= 18; i++)
                b.addSquare(new Square(i, 9, "item", "wall"));
            for (int i = 8; i <= 15; i++)
                b.addSquare(new Square(i, 15, "item", "wall"));
            for (int i = 3; i <= 8; i++)
                b.addSquare(new Square(5, i, "item", "wall"));
            for (int i = 10; i <= 14; i++)
                b.addSquare(new Square(8, i, "item", "wall"));
            for (int i = 10; i <= 14; i++)
                b.addSquare(new Square(15, i, "item", "wall"));
            for (int i = 3; i <= 8; i++)
                b.addSquare(new Square(18, i, "item", "wall"));
            b.addSquare(new Square(12, 3, "item", "wall"));
            b.addSquare(new Square(12, 4, "item", "wall"));
            for (int i = 6; i <= 8; i++)
                b.addSquare(new Square(12, i, "item", "wall"));
            for (int i = 9; i <= 11; i++)
                b.addSquare(new Square(i, 9, "item", "water"));
            for (int i = 6; i <= 8; i++)
                b.addSquare(new Square(i, 5, "item", "water"));
            for (int i = 6; i <= 8; i++)
                b.addSquare(new Square(i, 4, "item", "water"));
            b.addSquare(new Square(7, 3, "item", "water"));
            b.addSquare(new Square(8, 3, "item", "water"));
            b.addSquare(new Square(1, 16, "noun", "baba"));
            b.addSquare(new Square(1, 15, "operator", "is"));
            b.addSquare(new Square(1, 14, "property", "you"));
            b.addSquare(new Square(2, 16, "noun", "wall"));
            b.addSquare(new Square(2, 15, "operator", "is"));
            b.addSquare(new Square(2, 14, "property", "stop"));
            b.addSquare(new Square(7, 12, "noun", "water"));
            b.addSquare(new Square(7, 11, "operator", "is"));
            b.addSquare(new Square(7, 10, "property", "sink"));
            b.addSquare(new Square(14, 7, "noun", "rock"));
            b.addSquare(new Square(15, 7, "operator", "is"));
            b.addSquare(new Square(16, 7, "property", "push"));
            b.addSquare(new Square(14, 4, "noun", "flag"));
            b.addSquare(new Square(15, 4, "operator", "is"));
            b.addSquare(new Square(16, 4, "property", "win"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 4) {
            Board b = new Board(23, 15, 50);
            b.addSquare(new Square(7, 2, "item", "baba"));
            b.addSquare(new Square(7, 4, "item", "rock"));
            b.addSquare(new Square(7, 5, "item", "rock"));
            b.addSquare(new Square(7, 6, "item", "rock"));
            b.addSquare(new Square(18, 6, "item", "flag"));
            for (int i = 4; i <= 12; i++)
                b.addSquare(new Square(13, i, "item", "skull"));
            for (int i = 4; i <= 12; i++)
                b.addSquare(new Square(21, i, "item", "skull"));
            for (int i = 14; i <= 20; i++)
                b.addSquare(new Square(i, 4, "item", "skull"));
            for (int i = 14; i <= 20; i++)
                b.addSquare(new Square(i, 12, "item", "skull"));
            for (int i = 1; i <= 5; i++)
                b.addSquare(new Square(4, i, "item", "skull"));
            for (int i = 1; i <= 5; i++)
                b.addSquare(new Square(10, i, "item", "skull"));
            for (int i = 5; i <= 7; i++)
                b.addSquare(new Square(6, i, "item", "skull"));
            for (int i = 5; i <= 7; i++)
                b.addSquare(new Square(8, i, "item", "skull"));
            b.addSquare(new Square(5, 5, "item", "skull"));
            b.addSquare(new Square(9, 5, "item", "skull"));
            b.addSquare(new Square(1, 15, "noun", "flag"));
            b.addSquare(new Square(2, 15, "operator", "is"));
            b.addSquare(new Square(3, 15, "property", "win"));
            b.addSquare(new Square(1, 14, "noun", "baba"));
            b.addSquare(new Square(2, 14, "operator", "is"));
            b.addSquare(new Square(3, 14, "property", "you"));
            b.addSquare(new Square(3, 9, "noun", "rock"));
            b.addSquare(new Square(4, 9, "operator", "is"));
            b.addSquare(new Square(5, 9, "property", "push"));
            b.addSquare(new Square(15, 10, "noun", "skull"));
            b.addSquare(new Square(15, 9, "operator", "is"));
            b.addSquare(new Square(15, 8, "property", "defeat"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 5) {
            Board b = new Board(32, 18, 48);
            b.addSquare(new Square(15, 17, "item", "baba", KeyEvent.VK_LEFT));
            b.addSquare(new Square(13, 13, "item", "rock"));
            b.addSquare(new Square(26, 6, "item", "flag"));
            for (int i = 11; i <= 18; i++)
                b.addSquare(new Square(7, i, "item", "wall"));
            b.addSquare(new Square(13, 18, "item", "wall"));
            b.addSquare(new Square(8, 14, "item", "wall"));
            b.addSquare(new Square(9, 11, "item", "wall"));
            b.addSquare(new Square(12, 14, "item", "wall"));
            for (int i = 8; i <= 13; i++)
                b.addSquare(new Square(i, 12, "item", "wall"));
            for (int i = 14; i <= 17; i++)
                b.addSquare(new Square(i, 15, "item", "wall"));
            for (int i = 14; i <= 16; i++)
                b.addSquare(new Square(13, i, "item", "wall"));
            for (int i = 16; i <= 18; i++)
                b.addSquare(new Square(17, i, "item", "wall"));
            for (int i = 9; i <= 11; i++)
                b.addSquare(new Square(i, 3, "item", "wall"));
            for (int i = 9; i <= 11; i++)
                b.addSquare(new Square(i, 5, "item", "wall"));
            for (int i = 3; i <= 6; i++)
                b.addSquare(new Square(8, i, "item", "wall"));
            for (int i = 3; i <= 6; i++)
                b.addSquare(new Square(12, i, "item", "wall"));
            b.addSquare(new Square(1, 15, "item", "lava"));
            b.addSquare(new Square(1, 16, "item", "lava"));
            b.addSquare(new Square(2, 16, "item", "lava"));
            b.addSquare(new Square(4, 18, "item", "lava"));
            b.addSquare(new Square(5, 18, "item", "lava"));
            for (int i = 1; i <= 4; i++)
                b.addSquare(new Square(i, 17, "item", "lava"));
            for (int i = 12; i <= 18; i++)
                b.addSquare(new Square(i, 1, "item", "lava"));
            for (int i = 13; i <= 18; i++)
                b.addSquare(new Square(i, 2, "item", "lava"));
            for (int i = 14; i <= 19; i++)
                b.addSquare(new Square(i, 3, "item", "lava"));
            for (int i = 15; i <= 19; i++)
                b.addSquare(new Square(i, 4, "item", "lava"));
            for (int i = 15; i <= 20; i++)
                b.addSquare(new Square(i, 5, "item", "lava"));
            for (int i = 16; i <= 21; i++)
                b.addSquare(new Square(i, 6, "item", "lava"));
            for (int i = 16; i <= 21; i++)
                b.addSquare(new Square(i, 7, "item", "lava"));
            for (int i = 17; i <= 22; i++)
                b.addSquare(new Square(i, 8, "item", "lava"));
            for (int i = 17; i <= 23; i++)
                b.addSquare(new Square(i, 9, "item", "lava"));
            for (int i = 18; i <= 23; i++)
                b.addSquare(new Square(i, 10, "item", "lava"));
            for (int i = 18; i <= 23; i++)
                b.addSquare(new Square(i, 11, "item", "lava"));
            for (int i = 18; i <= 24; i++)
                b.addSquare(new Square(i, 12, "item", "lava"));
            for (int i = 19; i <= 24; i++)
                b.addSquare(new Square(i, 13, "item", "lava"));
            for (int i = 19; i <= 24; i++)
                b.addSquare(new Square(i, 14, "item", "lava"));
            for (int i = 19; i <= 25; i++)
                b.addSquare(new Square(i, 15, "item", "lava"));
            for (int i = 20; i <= 25; i++)
                b.addSquare(new Square(i, 16, "item", "lava"));
            for (int i = 20; i <= 26; i++)
                b.addSquare(new Square(i, 17, "item", "lava"));
            for (int i = 21; i <= 27; i++)
                b.addSquare(new Square(i, 18, "item", "lava"));
            for (int i = 30; i <= 32; i++)
                b.addSquare(new Square(i, 1, "item", "lava"));
            b.addSquare(new Square(31, 2, "item", "lava"));
            b.addSquare(new Square(32, 2, "item", "lava"));
            b.addSquare(new Square(32, 3, "item", "lava"));
            b.addSquare(new Square(1, 18, "noun", "wall"));
            b.addSquare(new Square(2, 18, "operator", "is"));
            b.addSquare(new Square(3, 18, "property", "stop"));
            b.addSquare(new Square(9, 15, "noun", "baba"));
            b.addSquare(new Square(10, 15, "operator", "is"));
            b.addSquare(new Square(11, 15, "property", "you"));
            b.addSquare(new Square(8, 11, "noun", "rock"));
            b.addSquare(new Square(8, 10, "operator", "is"));
            b.addSquare(new Square(8, 9, "property", "push"));
            b.addSquare(new Square(9, 6, "noun", "baba"));
            b.addSquare(new Square(10, 6, "operator", "is"));
            b.addSquare(new Square(11, 6, "property", "melt"));
            b.addSquare(new Square(9, 4, "noun", "lava"));
            b.addSquare(new Square(10, 4, "operator", "is"));
            b.addSquare(new Square(11, 4, "property", "hot"));
            b.addSquare(new Square(25, 4, "noun", "flag"));
            b.addSquare(new Square(26, 4, "operator", "is"));
            b.addSquare(new Square(27, 4, "property", "win"));
            b.addSquare(new Square(13, 8, "noun", "lava"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 6) {
            Board b = new Board(24, 14, 50);
            b.addSquare(new Square(9, 8, "item", "baba"));
            b.addSquare(new Square(18, 11, "item", "flag"));
            for (int i = 1; i <= 5; i++)
                b.addSquare(new Square(i, 8, "item", "rock"));
            for (int i = 9; i <= 14; i++)
                b.addSquare(new Square(5, i, "item", "rock"));
            for (int i = 13; i <= 24; i++)
                b.addSquare(new Square(i, 7, "item", "skull"));
            for (int i = 8; i <= 14; i++)
                b.addSquare(new Square(13, i, "item", "skull"));
            for (int i = 6; i <= 10; i++)
                b.addSquare(new Square(7, i, "item", "wall"));
            for (int i = 3; i <= 7; i++)
                b.addSquare(new Square(11, i, "item", "wall"));
            for (int i = 9; i <= 13; i++)
                b.addSquare(new Square(11, i, "item", "wall"));
            for (int i = 3; i <= 6; i++)
                b.addSquare(new Square(16, i, "item", "wall"));
            for (int i = 8; i <= 10; i++)
                b.addSquare(new Square(16, i, "item", "wall"));
            b.addSquare(new Square(16, 12, "item", "wall"));
            b.addSquare(new Square(16, 13, "item", "wall"));
            for (int i = 9; i <= 13; i++)
                b.addSquare(new Square(20, i, "item", "wall"));
            for (int i = 8; i <= 10; i++)
                b.addSquare(new Square(i, 6, "item", "wall"));
            for (int i = 8; i <= 10; i++)
                b.addSquare(new Square(i, 10, "item", "wall"));
            for (int i = 12; i <= 15; i++)
                b.addSquare(new Square(i, 3, "item", "wall"));
            for (int i = 17; i <= 19; i++)
                b.addSquare(new Square(i, 9, "item", "wall"));
            for (int i = 17; i <= 19; i++)
                b.addSquare(new Square(i, 13, "item", "wall"));
            b.addSquare(new Square(14, 8, "item", "wall"));
            b.addSquare(new Square(15, 8, "item", "wall"));
            b.addSquare(new Square(12, 13, "item", "wall"));
            b.addSquare(new Square(14, 13, "item", "wall"));
            b.addSquare(new Square(15, 13, "item", "wall"));
            b.addSquare(new Square(1, 14, "noun", "rock"));
            b.addSquare(new Square(2, 14, "operator", "is"));
            b.addSquare(new Square(3, 14, "property", "stop"));
            b.addSquare(new Square(1, 12, "noun", "skull"));
            b.addSquare(new Square(2, 12, "operator", "is"));
            b.addSquare(new Square(3, 12, "property", "defeat"));
            b.addSquare(new Square(1, 10, "noun", "flag"));
            b.addSquare(new Square(2, 10, "operator", "is"));
            b.addSquare(new Square(3, 10, "property", "win"));
            b.addSquare(new Square(9, 4, "noun", "baba"));
            b.addSquare(new Square(9, 3, "operator", "is"));
            b.addSquare(new Square(9, 2, "property", "you"));
            b.addSquare(new Square(13, 5, "noun", "wall"));
            b.addSquare(new Square(14, 5, "operator", "is"));
            b.addSquare(new Square(15, 5, "property", "stop"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 7) {
            Board b = new Board(24, 14, 50);
            b.addSquare(new Square(10, 7, "item", "baba", KeyEvent.VK_DOWN));
            b.addSquare(new Square(17, 10, "item", "flag"));
            b.addSquare(new Square(7, 6, "item", "grass"));
            b.addSquare(new Square(7, 9, "item", "grass"));
            b.addSquare(new Square(8, 5, "item", "grass"));
            b.addSquare(new Square(11, 4, "item", "grass"));
            b.addSquare(new Square(11, 11, "item", "grass"));
            b.addSquare(new Square(14, 4, "item", "grass"));
            b.addSquare(new Square(14, 9, "item", "grass"));
            b.addSquare(new Square(15, 8, "item", "grass"));
            b.addSquare(new Square(16, 5, "item", "grass"));
            b.addSquare(new Square(16, 7, "item", "grass"));
            b.addSquare(new Square(17, 8, "item", "grass"));
            b.addSquare(new Square(18, 7, "item", "grass"));
            for (int i = 5; i <= 7; i++)
                b.addSquare(new Square(13, i, "item", "grass"));
            for (int i = 3; i <= 12; i++)
                b.addSquare(new Square(6, i, "item", "wall"));
            for (int i = 3; i <= 12; i++)
                b.addSquare(new Square(19, i, "item", "wall"));
            for (int i = 7; i <= 18; i++)
                b.addSquare(new Square(i, 3, "item", "wall"));
            for (int i = 7; i <= 18; i++)
                b.addSquare(new Square(i, 12, "item", "wall"));
            b.addSquare(new Square(8, 10, "noun", "baba"));
            b.addSquare(new Square(9, 10, "operator", "is"));
            b.addSquare(new Square(10, 10, "property", "you"));
            b.addSquare(new Square(24, 8, "noun", "grass"));
            b.addSquare(new Square(24, 7, "operator", "is"));
            b.addSquare(new Square(24, 6, "property", "stop"));
            b.addSquare(new Square(15, 6, "noun", "flag"));
            b.addSquare(new Square(17, 6, "property", "win"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 8) {
            Board b = new Board(28, 16, 50);
            b.addSquare(new Square(8, 11, "item", "baba"));
            b.addSquare(new Square(25, 12, "item", "flag"));
            b.addSquare(new Square(24, 11, "item", "jelly"));
            b.addSquare(new Square(24, 13, "item", "jelly"));
            b.addSquare(new Square(26, 11, "item", "jelly"));
            b.addSquare(new Square(26, 13, "item", "jelly"));
            for (int i = 24; i <= 26; i++)
                b.addSquare(new Square(i, 10, "item", "jelly"));
            for (int i = 24; i <= 26; i++)
                b.addSquare(new Square(i, 14, "item", "jelly"));
            for (int i = 11; i <= 13; i++)
                b.addSquare(new Square(23, i, "item", "jelly"));
            for (int i = 11; i <= 13; i++)
                b.addSquare(new Square(27, i, "item", "jelly"));
            for (int i = 4; i <= 6; i++)
                b.addSquare(new Square(i, 16, "item", "water"));
            for (int i = 1; i <= 4; i++)
                b.addSquare(new Square(i, 15, "item", "water"));
            b.addSquare(new Square(1, 14, "item", "water"));
            b.addSquare(new Square(2, 14, "item", "water"));
            b.addSquare(new Square(1, 13, "item", "water"));
            b.addSquare(new Square(1, 3, "item", "water"));
            b.addSquare(new Square(2, 2, "item", "water"));
            b.addSquare(new Square(1, 2, "item", "water"));
            for (int i = 1; i <= 5; i++)
                b.addSquare(new Square(i, 1, "item", "water"));
            b.addSquare(new Square(7, 1, "item", "water"));
            b.addSquare(new Square(12, 1, "item", "water"));
            for (int i = 12; i <= 14; i++)
                b.addSquare(new Square(i, 16, "item", "water"));
            b.addSquare(new Square(17, 16, "item", "water"));
            b.addSquare(new Square(18, 16, "item", "water"));
            for (int i = 13; i <= 17; i++)
                b.addSquare(new Square(i, 15, "item", "water"));
            b.addSquare(new Square(19, 15, "item", "water"));
            for (int i = 14; i <= 17; i++)
                b.addSquare(new Square(i, 14, "item", "water"));
            b.addSquare(new Square(20, 14, "item", "water"));
            for (int i = 14; i <= 21; i++)
                b.addSquare(new Square(i, 13, "item", "water"));
            for (int i = 15; i <= 21; i++)
                b.addSquare(new Square(i, 12, "item", "water"));
            for (int i = 15; i <= 17; i++)
                b.addSquare(new Square(i, 11, "item", "water"));
            b.addSquare(new Square(21, 11, "item", "water"));
            b.addSquare(new Square(22, 11, "item", "water"));
            b.addSquare(new Square(15, 10, "item", "water"));
            b.addSquare(new Square(16, 10, "item", "water"));
            b.addSquare(new Square(22, 10, "item", "water"));
            b.addSquare(new Square(16, 9, "item", "water"));
            b.addSquare(new Square(22, 9, "item", "water"));
            b.addSquare(new Square(23, 9, "item", "water"));
            b.addSquare(new Square(16, 8, "item", "water"));
            for (int i = 22; i <= 24; i++)
                b.addSquare(new Square(i, 8, "item", "water"));
            b.addSquare(new Square(16, 7, "item", "water"));
            b.addSquare(new Square(17, 7, "item", "water"));
            for (int i = 17; i <= 23; i++)
                b.addSquare(new Square(i, 6, "item", "water"));
            b.addSquare(new Square(25, 6, "item", "water"));
            for (int i = 18; i <= 23; i++)
                b.addSquare(new Square(i, 5, "item", "water"));
            b.addSquare(new Square(25, 5, "item", "water"));
            b.addSquare(new Square(26, 5, "item", "water"));
            for (int i = 19; i <= 24; i++)
                b.addSquare(new Square(i, 4, "item", "water"));
            for (int i = 20; i <= 26; i++)
                b.addSquare(new Square(i, 3, "item", "water"));
            for (int i = 21; i <= 27; i++)
                b.addSquare(new Square(i, 2, "item", "water"));
            for (int i = 22; i <= 25; i++)
                b.addSquare(new Square(i, 1, "item", "water"));
            for (int i = 21; i <= 24; i++)
                b.addSquare(new Square(i, 7, "item", "water"));
            for (int i = 6; i <= 12; i++)
                b.addSquare(new Square(i, 4, "item", "wall"));
            for (int i = 9; i <= 12; i++)
                b.addSquare(new Square(i, 6, "item", "wall"));
            for (int i = 9; i <= 13; i++)
                b.addSquare(new Square(6, i, "item", "wall"));
            for (int i = 9; i <= 13; i++)
                b.addSquare(new Square(10, i, "item", "wall"));
            for (int i = 7; i <= 9; i++)
                b.addSquare(new Square(i, 13, "item", "wall"));
            b.addSquare(new Square(6, 5, "item", "wall"));
            b.addSquare(new Square(14, 5, "item", "wall"));
            b.addSquare(new Square(7, 9, "item", "wall"));
            b.addSquare(new Square(9, 9, "item", "wall"));
            b.addSquare(new Square(1, 16, "noun", "flag"));
            b.addSquare(new Square(2, 16, "operator", "is"));
            b.addSquare(new Square(3, 16, "property", "win"));
            b.addSquare(new Square(9, 5, "noun", "baba"));
            b.addSquare(new Square(10, 5, "operator", "is"));
            b.addSquare(new Square(11, 5, "property", "you"));
            b.addSquare(new Square(12, 5, "operator", "and"));
            b.addSquare(new Square(13, 5, "property", "sink"));
            b.addSquare(new Square(26, 16, "noun", "jelly"));
            b.addSquare(new Square(27, 16, "operator", "is"));
            b.addSquare(new Square(28, 16, "property", "sink"));
            b.addSquare(new Square(26, 1, "noun", "wall"));
            b.addSquare(new Square(27, 1, "operator", "is"));
            b.addSquare(new Square(28, 1, "property", "stop"));
            b.addSquare(new Square(19, 9, "noun", "wall"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 9) {
            Board b = new Board(28, 16, 50);
            b.addSquare(new Square(3, 10, "item", "crab"));
            b.addSquare(new Square(4, 5, "item", "flag"));
            b.addSquare(new Square(9, 10, "item", "skull"));
            b.addSquare(new Square(11, 10, "item", "star"));
            b.addSquare(new Square(14, 7, "item", "rock"));
            b.addSquare(new Square(22, 9, "item", "rock"));
            b.addSquare(new Square(24, 8, "item", "baba", KeyEvent.VK_LEFT));
            b.addSquare(new Square(24, 9, "item", "wall"));
            b.addSquare(new Square(24, 7, "item", "wall"));
            b.addSquare(new Square(22, 7, "item", "wall"));
            b.addSquare(new Square(22, 8, "item", "wall"));
            for (int i = 7; i < 14; i++)
                b.addSquare(new Square(5, i, "item", "wall"));
            for (int i = 6; i < 9; i++)
                b.addSquare(new Square(i, 13, "item", "wall"));
            for (int i = 5; i < 8; i++)
                b.addSquare(new Square(6, i, "item", "wall"));
            for (int i = 5; i < 8; i++)
                b.addSquare(new Square(8, i, "item", "wall"));
            for (int i = 11; i < 15; i++)
                b.addSquare(new Square(9, i, "item", "wall"));
            for (int i = 5; i < 10; i++)
                b.addSquare(new Square(9, i, "item", "wall"));
            for (int i = 10; i < 12; i++)
                b.addSquare(new Square(i, 9, "item", "wall"));
            for (int i = 10; i < 12; i++)
                b.addSquare(new Square(i, 11, "item", "wall"));
            for (int i = 10; i < 21; i++)
                b.addSquare(new Square(i, 14, "item", "wall"));
            for (int i = 6; i < 21; i++)
                b.addSquare(new Square(i, 4, "item", "wall"));
            for (int i = 4; i < 9; i++)
                b.addSquare(new Square(21, i, "item", "wall"));
            for (int i = 10; i < 15; i++)
                b.addSquare(new Square(21, i, "item", "wall"));
            for (int i = 22; i < 26; i++)
                b.addSquare(new Square(i, 10, "item", "wall"));
            for (int i = 22; i < 26; i++)
                b.addSquare(new Square(i, 6, "item", "wall"));
            for (int i = 6; i < 11; i++)
                b.addSquare(new Square(26, i, "item", "wall"));
            b.addSquare(new Square(6, 14, "noun", "skull"));
            b.addSquare(new Square(7, 14, "operator", "is"));
            b.addSquare(new Square(8, 14, "property", "defeat"));
            b.addSquare(new Square(7, 7, "noun", "baba"));
            b.addSquare(new Square(7, 6, "operator", "is"));
            b.addSquare(new Square(7, 5, "property", "you"));
            b.addSquare(new Square(3, 3, "noun", "flag"));
            b.addSquare(new Square(4, 3, "operator", "is"));
            b.addSquare(new Square(5, 3, "property", "win"));
            b.addSquare(new Square(14, 11, "noun", "star"));
            b.addSquare(new Square(15, 11, "operator", "is"));
            b.addSquare(new Square(16, 11, "property", "sink"));
            b.addSquare(new Square(14, 9, "noun", "rock"));
            b.addSquare(new Square(15, 9, "operator", "is"));
            b.addSquare(new Square(16, 9, "property", "push"));
            b.addSquare(new Square(22, 11, "noun", "wall"));
            b.addSquare(new Square(23, 11, "operator", "is"));
            b.addSquare(new Square(24, 11, "property", "stop"));
            b.addSquare(new Square(7, 10, "noun", "crab"));
            b.addSquare(new Square(16, 7, "operator", "and"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 10) {
            Board b = new Board(24, 14, 50);
            b.addSquare(new Square(7, 7, "item", "baba"));
            b.addSquare(new Square(18, 7, "item", "flag"));
            b.addSquare(new Square(11, 12, "item", "pillar"));
            b.addSquare(new Square(12, 3, "item", "pillar"));
            b.addSquare(new Square(13, 11, "item", "pillar"));
            b.addSquare(new Square(18, 2, "item", "pillar"));
            b.addSquare(new Square(19, 13, "item", "pillar"));
            b.addSquare(new Square(9, 7, "item", "pillar"));
            for (int i = 5; i <= 9; i++)
                b.addSquare(new Square(5, i, "item", "wall"));
            for (int i = 6; i <= 9; i++)
                b.addSquare(new Square(i, 5, "item", "wall"));
            for (int i = 6; i <= 9; i++)
                b.addSquare(new Square(i, 9, "item", "wall"));
            b.addSquare(new Square(9, 6, "item", "wall"));
            b.addSquare(new Square(9, 8, "item", "wall"));
            for (int i = 17; i <= 19; i++)
                b.addSquare(new Square(i, 5, "item", "star"));
            for (int i = 17; i <= 19; i++)
                b.addSquare(new Square(i, 9, "item", "star"));
            for (int i = 5; i <= 9; i++)
                b.addSquare(new Square(16, i, "item", "star"));
            for (int i = 5; i <= 9; i++)
                b.addSquare(new Square(20, i, "item", "star"));
            b.addSquare(new Square(1, 14, "noun", "flag"));
            b.addSquare(new Square(2, 14, "operator", "is"));
            b.addSquare(new Square(3, 14, "property", "win"));
            b.addSquare(new Square(1, 8, "noun", "wall"));
            b.addSquare(new Square(1, 7, "operator", "is"));
            b.addSquare(new Square(1, 6, "property", "stop"));
            b.addSquare(new Square(6, 12, "noun", "pillar"));
            b.addSquare(new Square(7, 12, "operator", "is"));
            b.addSquare(new Square(8, 12, "property", "push"));
            b.addSquare(new Square(6, 2, "noun", "baba"));
            b.addSquare(new Square(7, 2, "operator", "is"));
            b.addSquare(new Square(8, 2, "property", "you"));
            b.addSquare(new Square(22, 1, "noun", "star"));
            b.addSquare(new Square(23, 1, "operator", "is"));
            b.addSquare(new Square(24, 1, "property", "defeat"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 11) {
            Board b = new Board(15, 7, 100);
            b.addSquare(new Square(2, 4, "item", "baba"));
            b.addSquare(new Square(12, 4, "item", "flag"));
            for (int i = 10; i <= 15; i++)
                b.addSquare(new Square(i, 2, "item", "wall"));
            for (int i = 10; i <= 15; i++)
                b.addSquare(new Square(i, 6, "item", "wall"));
            for (int i = 3; i <= 5; i++)
                b.addSquare(new Square(10, i, "item", "wall"));
            for (int i = 3; i <= 5; i++)
                b.addSquare(new Square(14, i, "item", "wall"));
            b.addSquare(new Square(5, 6, "noun", "baba"));
            b.addSquare(new Square(6, 6, "operator", "is"));
            b.addSquare(new Square(7, 6, "property", "you"));
            b.addSquare(new Square(5, 2, "noun", "flag"));
            b.addSquare(new Square(6, 2, "operator", "is"));
            b.addSquare(new Square(7, 2, "property", "win"));
            b.addSquare(new Square(15, 5, "noun", "wall"));
            b.addSquare(new Square(15, 4, "operator", "is"));
            b.addSquare(new Square(15, 3, "property", "stop"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 12) {
            Board b = new Board(28, 16, 50);
            b.addSquare(new Square(10, 9, "item", "baba"));
            b.addSquare(new Square(20, 5, "item", "flag"));
            b.addSquare(new Square(8, 9, "item", "key"));
            b.addSquare(new Square(15, 9, "item", "key"));
            b.addSquare(new Square(12, 9, "item", "door"));
            b.addSquare(new Square(17, 9, "item", "door"));
            b.addSquare(new Square(20, 7, "item", "door"));
            b.addSquare(new Square(14, 9, "item", "rock"));
            b.addSquare(new Square(12, 10, "item", "wall"));
            b.addSquare(new Square(12, 8, "item", "wall"));
            b.addSquare(new Square(21, 7, "item", "wall"));
            for (int i = 7; i < 16; i++)
                b.addSquare(new Square(6, i, "item", "wall"));
            for (int i = 7; i < 10; i++)
                b.addSquare(new Square(i, 15, "item", "wall"));
            for (int i = 7; i < 10; i++)
                b.addSquare(new Square(i, 13, "item", "wall"));
            for (int i = 12; i < 16; i++)
                b.addSquare(new Square(10, i, "item", "wall"));
            for (int i = 7; i < 13; i++)
                b.addSquare(new Square(i, 11, "item", "wall"));
            for (int i = 7; i < 13; i++)
                b.addSquare(new Square(i, 7, "item", "wall"));
            for (int i = 12; i < 16; i++)
                b.addSquare(new Square(18, i, "item", "wall"));
            for (int i = 12; i < 16; i++)
                b.addSquare(new Square(22, i, "item", "wall"));
            for (int i = 19; i < 22; i++)
                b.addSquare(new Square(i, 15, "item", "wall"));
            for (int i = 19; i < 22; i++)
                b.addSquare(new Square(i, 13, "item", "wall"));
            for (int i = 4; i < 9; i++)
                b.addSquare(new Square(17, i, "item", "wall"));
            for (int i = 10; i < 12; i++)
                b.addSquare(new Square(17, i, "item", "wall"));
            for (int i = 7; i < 12; i++)
                b.addSquare(new Square(23, i, "item", "wall"));
            for (int i = 2; i < 5; i++)
                b.addSquare(new Square(18, i, "item", "wall"));
            for (int i = 2; i < 8; i++)
                b.addSquare(new Square(22, i, "item", "wall"));
            for (int i = 18; i < 23; i++)
                b.addSquare(new Square(i, 11, "item", "wall"));
            for (int i = 18; i < 20; i++)
                b.addSquare(new Square(i, 7, "item", "wall"));
            for (int i = 19; i < 22; i++)
                b.addSquare(new Square(i, 2, "item", "wall"));
            b.addSquare(new Square(7, 14, "noun", "baba"));
            b.addSquare(new Square(8, 14, "operator", "is"));
            b.addSquare(new Square(9, 14, "property", "you"));
            b.addSquare(new Square(7, 12, "noun", "wall"));
            b.addSquare(new Square(8, 12, "operator", "is"));
            b.addSquare(new Square(9, 12, "property", "stop"));
            b.addSquare(new Square(19, 14, "noun", "door"));
            b.addSquare(new Square(20, 14, "operator", "is"));
            b.addSquare(new Square(21, 14, "property", "stop"));
            b.addSquare(new Square(19, 12, "noun", "door"));
            b.addSquare(new Square(20, 12, "operator", "is"));
            b.addSquare(new Square(21, 12, "property", "shut"));
            b.addSquare(new Square(19, 3, "noun", "flag"));
            b.addSquare(new Square(20, 3, "operator", "is"));
            b.addSquare(new Square(21, 3, "property", "win"));
            b.addSquare(new Square(10, 5, "noun", "key"));
            b.addSquare(new Square(11, 5, "operator", "is"));
            b.addSquare(new Square(12, 5, "property", "open"));
            b.addSquare(new Square(10, 4, "operator", "is"));
            b.addSquare(new Square(10, 3, "property", "push"));
            b.addSquare(new Square(20, 9, "noun", "rock"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 13) {
            Board b = new Board(24, 15, 50);
            for (int i = 2; i <= 15; i++)
                b.addSquare(new Square(5, i, "item", "wall"));
            for (int i = 2; i <= 15; i++)
                if (i != 6)
                    b.addSquare(new Square(11, i, "item", "wall"));
            for (int i = 6; i <= 10; i++)
                b.addSquare(new Square(i, 2, "item", "wall"));
            for (int i = 6; i <= 10; i++)
                b.addSquare(new Square(i, 12, "item", "wall"));
            for (int i = 12; i <= 20; i++)
                if (i != 16)
                    b.addSquare(new Square(i, 4, "item", "wall"));
            for (int i = 12; i <= 20; i++)
                b.addSquare(new Square(i, 12, "item", "wall"));
            for (int i = 5; i <= 11; i++)
                b.addSquare(new Square(20, i, "item", "wall"));
            b.addSquare(new Square(16, 4, "item", "door"));
            b.addSquare(new Square(16, 2, "item", "flag"));
            b.addSquare(new Square(8, 3, "item", "baba"));
            b.addSquare(new Square(11, 6, "item", "door"));
            b.addSquare(new Square(7, 5, "item", "key"));
            b.addSquare(new Square(7, 15, "noun", "baba"));
            b.addSquare(new Square(8, 15, "operator", "is"));
            b.addSquare(new Square(9, 15, "property", "you"));
            b.addSquare(new Square(7, 14, "noun", "door"));
            b.addSquare(new Square(8, 14, "operator", "is"));
            b.addSquare(new Square(9, 14, "property", "stop"));
            b.addSquare(new Square(7, 13, "noun", "wall"));
            b.addSquare(new Square(8, 13, "operator", "is"));
            b.addSquare(new Square(9, 13, "property", "stop"));
            b.addSquare(new Square(7, 9, "noun", "key"));
            b.addSquare(new Square(9, 9, "property", "open"));
            b.addSquare(new Square(7, 7, "noun", "key"));
            b.addSquare(new Square(9, 7, "property", "push"));
            b.addSquare(new Square(9, 5, "operator", "is"));
            b.addSquare(new Square(15, 9, "noun", "door"));
            b.addSquare(new Square(16, 9, "operator", "is"));
            b.addSquare(new Square(17, 9, "property", "shut"));
            b.addSquare(new Square(18, 2, "noun", "flag"));
            b.addSquare(new Square(19, 2, "operator", "is"));
            b.addSquare(new Square(20, 2, "property", "win"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 14) {
            Board b = new Board(27, 16, 50);
            b.addSquare(new Square(12, 10, "item", "baba"));
            b.addSquare(new Square(20, 10, "item", "flag"));
            b.addSquare(new Square(10, 8, "item", "key"));
            b.addSquare(new Square(17, 10, "item", "door"));
            b.addSquare(new Square(8, 11, "item", "grass"));
            b.addSquare(new Square(7, 9, "item", "grass"));
            b.addSquare(new Square(6, 7, "item", "grass"));
            for (int i = 7; i < 14; i++)
                b.addSquare(new Square(5, i, "item", "grass"));
            for (int i = 6; i < 8; i++)
                b.addSquare(new Square(i, 13, "item", "grass"));
            for (int i = 3; i < 9; i++)
                b.addSquare(new Square(9, i, "item", "grass"));
            for (int i = 3; i < 6; i++)
                b.addSquare(new Square(10, i, "item", "grass"));
            for (int i = 3; i < 8; i++)
                b.addSquare(new Square(13, i, "item", "grass"));
            for (int i = 11; i < 13; i++)
                b.addSquare(new Square(i, 3, "item", "grass"));
            for (int i = 11; i < 13; i++)
                b.addSquare(new Square(i, 7, "item", "grass"));
            for (int i = 7; i < 10; i++)
                b.addSquare(new Square(17, i, "item", "grass"));
            for (int i = 11; i < 14; i++)
                b.addSquare(new Square(17, i, "item", "grass"));
            for (int i = 7; i < 14; i++)
                b.addSquare(new Square(23, i, "item", "grass"));
            for (int i = 18; i < 23; i++)
                b.addSquare(new Square(i, 13, "item", "grass"));
            for (int i = 18; i < 23; i++)
                b.addSquare(new Square(i, 7, "item", "grass"));
            b.addSquare(new Square(11, 14, "noun", "baba"));
            b.addSquare(new Square(11, 13, "operator", "is"));
            b.addSquare(new Square(11, 12, "property", "you"));
            b.addSquare(new Square(14, 14, "noun", "door"));
            b.addSquare(new Square(14, 13, "operator", "is"));
            b.addSquare(new Square(14, 12, "property", "shut"));
            b.addSquare(new Square(6, 12, "noun", "key"));
            b.addSquare(new Square(6, 11, "operator", "is"));
            b.addSquare(new Square(6, 10, "property", "push"));
            b.addSquare(new Square(6, 9, "operator", "and"));
            b.addSquare(new Square(6, 8, "property", "open"));
            b.addSquare(new Square(11, 6, "noun", "flag"));
            b.addSquare(new Square(12, 5, "operator", "is"));
            b.addSquare(new Square(12, 4, "property", "win"));
            b.addSquare(new Square(22, 12, "noun", "grass"));
            b.addSquare(new Square(22, 11, "operator", "and"));
            b.addSquare(new Square(22, 10, "noun", "door"));
            b.addSquare(new Square(22, 9, "operator", "is"));
            b.addSquare(new Square(22, 8, "property", "stop"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 15) {
            Board b = new Board(15, 8, 100);
            b.addSquare(new Square(4, 5, "item", "baba"));
            b.addSquare(new Square(8, 5, "item", "flag"));
            b.addSquare(new Square(4, 1, "item", "wall"));
            b.addSquare(new Square(12, 1, "item", "wall"));
            for (int i = 1; i <= 4; i++)
                b.addSquare(new Square(i, 2, "item", "wall"));
            for (int i = 12; i <= 15; i++)
                b.addSquare(new Square(i, 2, "item", "wall"));
            for (int i = 6; i <= 10; i++)
                b.addSquare(new Square(i, 8, "item", "wall"));
            b.addSquare(new Square(13, 1, "noun", "flag"));
            b.addSquare(new Square(14, 1, "operator", "is"));
            b.addSquare(new Square(15, 1, "property", "win"));
            b.addSquare(new Square(15, 5, "noun", "wall"));
            b.addSquare(new Square(15, 4, "operator", "is"));
            b.addSquare(new Square(15, 3, "property", "stop"));
            b.addSquare(new Square(7, 7, "noun", "rock"));
            b.addSquare(new Square(8, 7, "operator", "is"));
            b.addSquare(new Square(9, 7, "noun", "rock"));
            b.addSquare(new Square(7, 3, "noun", "flag"));
            b.addSquare(new Square(8, 3, "operator", "is"));
            b.addSquare(new Square(9, 3, "noun", "rock"));
            b.addSquare(new Square(1, 1, "noun", "baba"));
            b.addSquare(new Square(2, 1, "operator", "is"));
            b.addSquare(new Square(3, 1, "property", "you"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 16) {
            Board b = new Board(24, 16, 50);
            b.addSquare(new Square(9, 5, "item", "baba", KeyEvent.VK_DOWN));
            b.addSquare(new Square(7, 5, "item", "flag"));
            b.addSquare(new Square(11, 5, "item", "crab"));
            b.addSquare(new Square(16, 6, "item", "door"));
            b.addSquare(new Square(11, 11, "item", "grass"));
            for (int i = 3; i < 12; i++)
                b.addSquare(new Square(5, i, "item", "grass"));
            for (int i = 6; i < 10; i++)
                b.addSquare(new Square(i, 11, "item", "grass"));
            for (int i = 12; i < 14; i++)
                b.addSquare(new Square(9, i, "item", "grass"));
            for (int i = 10; i < 12; i++)
                b.addSquare(new Square(i, 13, "item", "grass"));
            for (int i = 14; i < 16; i++)
                b.addSquare(new Square(11, i, "item", "grass"));
            for (int i = 12; i < 16; i++)
                b.addSquare(new Square(i, 15, "item", "grass"));
            for (int i = 11; i < 15; i++)
                b.addSquare(new Square(15, i, "item", "grass"));
            for (int i = 11; i < 13; i++)
                b.addSquare(new Square(13, i, "item", "grass"));
            for (int i = 11; i < 13; i++)
                b.addSquare(new Square(14, i, "item", "grass"));
            for (int i = 7; i < 12; i++)
                b.addSquare(new Square(16, i, "item", "grass"));
            for (int i = 3; i < 6; i++)
                b.addSquare(new Square(16, i, "item", "grass"));
            for (int i = 17; i < 21; i++)
                b.addSquare(new Square(i, 8, "item", "grass"));
            for (int i = 17; i < 21; i++)
                b.addSquare(new Square(i, 4, "item", "grass"));
            for (int i = 5; i < 8; i++)
                b.addSquare(new Square(20, i, "item", "grass"));
            for (int i = 6; i < 16; i++)
                b.addSquare(new Square(i, 3, "item", "grass"));
            b.addSquare(new Square(18, 6, "noun", "crab"));
            b.addSquare(new Square(1, 1, "noun", "baba"));
            b.addSquare(new Square(2, 1, "operator", "is"));
            b.addSquare(new Square(3, 1, "property", "you"));
            b.addSquare(new Square(1, 16, "noun", "grass"));
            b.addSquare(new Square(2, 16, "operator", "is"));
            b.addSquare(new Square(3, 16, "property", "stop"));
            b.addSquare(new Square(6, 8, "noun", "flag"));
            b.addSquare(new Square(7, 8, "operator", "is"));
            b.addSquare(new Square(8, 8, "property", "push"));
            b.addSquare(new Square(13, 8, "noun", "baba"));
            b.addSquare(new Square(13, 7, "operator", "is"));
            b.addSquare(new Square(13, 6, "property", "open"));
            b.addSquare(new Square(20, 1, "noun", "door"));
            b.addSquare(new Square(21, 1, "operator", "is"));
            b.addSquare(new Square(22, 1, "property", "shut"));
            b.addSquare(new Square(23, 1, "operator", "and"));
            b.addSquare(new Square(24, 1, "property", "stop"));
            b.addSquare(new Square(10, 12, "noun", "baba"));
            b.addSquare(new Square(11, 12, "operator", "is"));
            b.addSquare(new Square(13, 13, "operator", "is"));
            b.addSquare(new Square(13, 14, "operator", "is"));
            b.addSquare(new Square(14, 13, "property", "defeat"));
            b.addSquare(new Square(14, 14, "property", "win"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 17) {
            Board b = new Board(28, 16, 50);
            b.addSquare(new Square(16, 5, "item", "baba", KeyEvent.VK_DOWN));
            b.addSquare(new Square(12, 7, "item", "key"));
            b.addSquare(new Square(15, 11, "item", "door"));
            b.addSquare(new Square(17, 7, "item", "star"));
            b.addSquare(new Square(4, 14, "item", "wall"));
            b.addSquare(new Square(26, 12, "item", "wall"));
            b.addSquare(new Square(18, 9, "item", "wall"));
            for (int i = 4; i < 8; i++)
                b.addSquare(new Square(i, 16, "item", "wall"));
            for (int i = 1; i < 6; i++)
                b.addSquare(new Square(i, 15, "item", "wall"));
            for (int i = 1; i < 5; i++)
                b.addSquare(new Square(i, 13, "item", "wall"));
            for (int i = 1; i < 3; i++)
                b.addSquare(new Square(i, 12, "item", "wall"));
            for (int i = 10; i < 12; i++)
                b.addSquare(new Square(1, i, "item", "wall"));
            for (int i = 23; i < 26; i++)
                b.addSquare(new Square(i, 16, "item", "wall"));
            for (int i = 25; i < 29; i++)
                b.addSquare(new Square(i, 15, "item", "wall"));
            for (int i = 23; i < 26; i++)
                b.addSquare(new Square(i, 14, "item", "wall"));
            for (int i = 22; i < 24; i++)
                b.addSquare(new Square(i, 13, "item", "wall"));
            for (int i = 25; i < 29; i++)
                b.addSquare(new Square(i, 13, "item", "wall"));
            for (int i = 9; i < 13; i++)
                b.addSquare(new Square(27, i, "item", "wall"));
            for (int i = 7; i < 10; i++)
                b.addSquare(new Square(28, i, "item", "wall"));
            for (int i = 2; i < 12; i++)
                b.addSquare(new Square(9, i, "item", "wall"));
            for (int i = 2; i < 12; i++)
                b.addSquare(new Square(20, i, "item", "wall"));
            for (int i = 10; i < 20; i++)
                b.addSquare(new Square(i, 2, "item", "wall"));
            for (int i = 10; i < 15; i++)
                b.addSquare(new Square(i, 11, "item", "wall"));
            for (int i = 16; i < 20; i++)
                b.addSquare(new Square(i, 11, "item", "wall"));
            b.addSquare(new Square(1, 16, "noun", "baba"));
            b.addSquare(new Square(2, 16, "operator", "is"));
            b.addSquare(new Square(3, 16, "property", "you"));
            b.addSquare(new Square(1, 14, "noun", "door"));
            b.addSquare(new Square(2, 14, "operator", "is"));
            b.addSquare(new Square(3, 14, "property", "stop"));
            b.addSquare(new Square(22, 15, "noun", "key"));
            b.addSquare(new Square(23, 15, "operator", "is"));
            b.addSquare(new Square(24, 15, "property", "defeat"));
            b.addSquare(new Square(10, 10, "noun", "star"));
            b.addSquare(new Square(11, 10, "operator", "is"));
            b.addSquare(new Square(12, 10, "property", "push"));
            b.addSquare(new Square(17, 10, "noun", "wall"));
            b.addSquare(new Square(18, 10, "operator", "is"));
            b.addSquare(new Square(19, 10, "property", "stop"));
            b.addSquare(new Square(11, 5, "noun", "key"));
            b.addSquare(new Square(12, 5, "operator", "is"));
            b.addSquare(new Square(13, 5, "property", "open"));
            b.addSquare(new Square(17, 5, "operator", "is"));
            b.addSquare(new Square(18, 5, "property", "shut"));
            b.addSquare(new Square(14, 14, "operator", "is"));
            b.addSquare(new Square(16, 14, "property", "win"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 18) {
            Board b = new Board(24, 16, 50);
            for (int i = 3; i <= 14; i++)
                b.addSquare(new Square(6, i, "item", "wall"));
            for (int i = 7; i <= 18; i++)
                b.addSquare(new Square(i, 3, "item", "wall"));
            for (int i = 7; i <= 15; i++)
                b.addSquare(new Square(i, 14, "item", "wall"));
            for (int i = 9; i <= 14; i++)
                b.addSquare(new Square(16, i, "item", "wall"));
            for (int i = 17; i <= 18; i++)
                b.addSquare(new Square(i, 9, "item", "wall"));
            for (int i = 3; i <= 9; i++)
                b.addSquare(new Square(19, i, "item", "wall"));
            b.addSquare(new Square(10, 7, "noun", "rock"));
            b.addSquare(new Square(10, 9, "item", "baba"));
            b.addSquare(new Square(10, 11, "noun", "baba"));
            b.addSquare(new Square(11, 7, "operator", "is"));
            b.addSquare(new Square(12, 9, "item", "rock"));
            b.addSquare(new Square(12, 11, "property", "you"));
            b.addSquare(new Square(11, 11, "operator", "is"));
            b.addSquare(new Square(18, 5, "property", "stop"));
            b.addSquare(new Square(18, 6, "operator", "is"));
            b.addSquare(new Square(18, 7, "noun", "wall"));
            b.addSquare(new Square(18, 11, "property", "win"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 19) {
            Board b = new Board(24, 14, 50);
            b.addSquare(new Square(3, 4, "item", "flag"));
            b.addSquare(new Square(5, 4, "item", "key"));
            b.addSquare(new Square(10, 6, "item", "water"));
            b.addSquare(new Square(10, 7, "item", "water"));
            b.addSquare(new Square(10, 8, "item", "water"));
            b.addSquare(new Square(10, 9, "item", "water"));
            b.addSquare(new Square(11, 6, "item", "water"));
            b.addSquare(new Square(11, 8, "item", "water"));
            b.addSquare(new Square(11, 9, "item", "water"));
            b.addSquare(new Square(12, 6, "item", "water"));
            b.addSquare(new Square(12, 9, "item", "water"));
            b.addSquare(new Square(13, 6, "item", "water"));
            b.addSquare(new Square(13, 9, "item", "water"));
            b.addSquare(new Square(14, 6, "item", "water"));
            b.addSquare(new Square(14, 7, "item", "water"));
            b.addSquare(new Square(14, 8, "item", "water"));
            b.addSquare(new Square(14, 9, "item", "water"));
            b.addSquare(new Square(12, 11, "item", "baba"));
            b.addSquare(new Square(19, 2, "item", "water"));
            b.addSquare(new Square(20, 2, "item", "water"));
            b.addSquare(new Square(21, 2, "item", "water"));
            b.addSquare(new Square(22, 2, "item", "water"));
            b.addSquare(new Square(23, 2, "item", "water"));
            b.addSquare(new Square(24, 2, "item", "water"));
            b.addSquare(new Square(19, 1, "item", "water"));
            b.addSquare(new Square(20, 1, "noun", "water"));
            b.addSquare(new Square(21, 1, "operator", "is"));
            b.addSquare(new Square(22, 1, "property", "stop"));
            b.addSquare(new Square(23, 1, "operator", "and"));
            b.addSquare(new Square(24, 1, "property", "shut"));
            b.addSquare(new Square(12, 7, "property", "win"));
            b.addSquare(new Square(9, 6, "property", "open"));
            b.addSquare(new Square(9, 7, "operator", "and"));
            b.addSquare(new Square(9, 8, "property", "you"));
            b.addSquare(new Square(9, 9, "operator", "is"));
            b.addSquare(new Square(3, 5, "noun", "flag"));
            b.addSquare(new Square(5, 5, "noun", "key"));
            b.addSquare(new Square(9, 10, "noun", "baba"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 20) {
            Board b = new Board(15, 8, 100);
            b.addSquare(new Square(10, 7, "item", "baba"));
            b.addSquare(new Square(13, 4, "item", "water"));
            b.addSquare(new Square(13, 4, "item", "flag"));
            b.addSquare(new Square(12, 4, "item", "lava"));
            b.addSquare(new Square(14, 4, "item", "lava"));
            for (int i = 12; i <= 14; i++)
                b.addSquare(new Square(i, 5, "item", "lava"));
            for (int i = 12; i <= 14; i++)
                b.addSquare(new Square(i, 3, "item", "lava"));
            b.addSquare(new Square(9, 4, "item", "pillar"));
            b.addSquare(new Square(9, 5, "item", "pillar"));
            b.addSquare(new Square(10, 4, "item", "pillar"));
            b.addSquare(new Square(10, 5, "item", "pillar"));
            b.addSquare(new Square(4, 8, "item", "wall"));
            for (int i = 1; i <= 7; i++)
                b.addSquare(new Square(i, 7, "item", "wall"));
            for (int i = 4; i <= 6; i++)
                b.addSquare(new Square(2, i, "item", "wall"));
            for (int i = 4; i <= 6; i++)
                b.addSquare(new Square(3, i, "item", "wall"));
            for (int i = 5; i <= 6; i++)
                b.addSquare(new Square(7, i, "item", "wall"));
            for (int i = 1; i <= 4; i++)
                b.addSquare(new Square(i, 3, "item", "wall"));
            for (int i = 1; i <= 7; i++)
                if (i != 6)
                    b.addSquare(new Square(i, 2, "item", "wall"));
            for (int i = 4; i <= 7; i++)
                b.addSquare(new Square(i, 1, "item", "wall"));
            b.addSquare(new Square(1, 8, "noun", "baba"));
            b.addSquare(new Square(2, 8, "operator", "is"));
            b.addSquare(new Square(3, 8, "property", "you"));
            b.addSquare(new Square(5, 8, "noun", "wall"));
            b.addSquare(new Square(6, 8, "operator", "is"));
            b.addSquare(new Square(7, 8, "property", "stop"));
            b.addSquare(new Square(1, 6, "noun", "flag"));
            b.addSquare(new Square(1, 5, "operator", "is"));
            b.addSquare(new Square(1, 4, "property", "melt"));
            b.addSquare(new Square(1, 1, "noun", "pillar"));
            b.addSquare(new Square(2, 1, "operator", "is"));
            b.addSquare(new Square(3, 1, "property", "pull"));
            b.addSquare(new Square(4, 6, "noun", "water"));
            b.addSquare(new Square(4, 5, "operator", "is"));
            b.addSquare(new Square(5, 4, "property", "hot"));
            b.addSquare(new Square(5, 6, "noun", "lava"));
            b.addSquare(new Square(5, 5, "operator", "is"));
            b.addSquare(new Square(6, 4, "property", "defeat"));
            b.addSquare(new Square(6, 6, "noun", "flag"));
            b.addSquare(new Square(6, 5, "operator", "is"));
            b.addSquare(new Square(7, 4, "property", "win"));
            b.addSquare(new Square(6, 3, "operator", "and"));
            b.addSquare(new Square(6, 2, "property", "pull"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 21) {
            Board b = new Board(28, 16, 50);
            b.addSquare(new Square(15, 10, "item", "baba", KeyEvent.VK_DOWN));
            b.addSquare(new Square(9, 9, "item", "key"));
            b.addSquare(new Square(20, 9, "item", "door"));
            b.addSquare(new Square(24, 9, "item", "flag"));
            b.addSquare(new Square(1, 8, "item", "water"));
            b.addSquare(new Square(16, 14, "item", "water"));
            b.addSquare(new Square(24, 2, "item", "water"));
            b.addSquare(new Square(4, 5, "item", "water"));
            for (int i = 1; i <= 2; i++)
                b.addSquare(new Square(i, 7, "item", "water"));
            for (int i = 1; i <= 4; i++)
                b.addSquare(new Square(i, 6, "item", "water"));
            for (int i = 1; i <= 5; i++)
                b.addSquare(new Square(i, 4, "item", "water"));
            for (int i = 4; i <= 5; i++)
                b.addSquare(new Square(i, 3, "item", "water"));
            for (int i = 1; i <= 21; i++)
                if (i != 7)
                    b.addSquare(new Square(i, 2, "item", "water"));
            for (int i = 6; i <= 20; i++)
                b.addSquare(new Square(i, 1, "item", "water"));
            for (int i = 8; i <= 9; i++)
                b.addSquare(new Square(i, 3, "item", "water"));
            for (int i = 4; i <= 8; i++)
                b.addSquare(new Square(9, i, "item", "water"));
            for (int i = 20; i <= 23; i++)
                b.addSquare(new Square(i, 16, "item", "water"));
            for (int i = 19; i <= 22; i++)
                b.addSquare(new Square(i, 15, "item", "water"));
            for (int i = 19; i <= 20; i++)
                b.addSquare(new Square(i, 14, "item", "water"));
            for (int i = 27; i <= 28; i++)
                b.addSquare(new Square(i, 16, "item", "water"));
            for (int i = 25; i <= 28; i++)
                b.addSquare(new Square(i, 15, "item", "water"));
            for (int i = 24; i <= 28; i++)
                b.addSquare(new Square(i, 14, "item", "water"));
            for (int i = 24; i <= 28; i++)
                b.addSquare(new Square(i, 13, "item", "water"));
            for (int i = 23; i <= 28; i++)
                b.addSquare(new Square(i, 12, "item", "water"));
            for (int i = 22; i <= 28; i++)
                b.addSquare(new Square(i, 11, "item", "water"));
            for (int i = 20; i <= 22; i++)
                b.addSquare(new Square(i, 10, "item", "water"));
            for (int i = 26; i <= 28; i++)
                b.addSquare(new Square(i, 10, "item", "water"));
            for (int i = 26; i <= 28; i++)
                b.addSquare(new Square(i, 9, "item", "water"));
            for (int i = 26; i <= 28; i++)
                b.addSquare(new Square(i, 8, "item", "water"));
            for (int i = 20; i <= 22; i++)
                b.addSquare(new Square(i, 8, "item", "water"));
            for (int i = 20; i <= 27; i++)
                b.addSquare(new Square(i, 7, "item", "water"));
            for (int i = 20; i <= 26; i++)
                b.addSquare(new Square(i, 6, "item", "water"));
            for (int i = 20; i <= 24; i++)
                b.addSquare(new Square(i, 5, "item", "water"));
            for (int i = 20; i <= 23; i++)
                b.addSquare(new Square(i, 4, "item", "water"));
            for (int i = 20; i <= 22; i++)
                b.addSquare(new Square(i, 3, "item", "water"));
            for (int i = 26; i <= 27; i++)
                b.addSquare(new Square(i, 2, "item", "water"));
            for (int i = 26; i <= 27; i++)
                b.addSquare(new Square(i, 3, "item", "water"));
            b.addSquare(new Square(1, 16, "noun", "key"));
            b.addSquare(new Square(2, 16, "operator", "is"));
            b.addSquare(new Square(3, 16, "property", "push"));
            b.addSquare(new Square(4, 16, "operator", "and"));
            b.addSquare(new Square(5, 16, "property", "fall"));
            b.addSquare(new Square(1, 1, "noun", "water"));
            b.addSquare(new Square(2, 1, "operator", "and"));
            b.addSquare(new Square(3, 1, "noun", "door"));
            b.addSquare(new Square(4, 1, "operator", "is"));
            b.addSquare(new Square(5, 1, "property", "stop"));
            b.addSquare(new Square(1, 5, "noun", "flag"));
            b.addSquare(new Square(2, 5, "operator", "is"));
            b.addSquare(new Square(3, 5, "property", "win"));
            b.addSquare(new Square(1, 3, "noun", "door"));
            b.addSquare(new Square(2, 3, "operator", "is"));
            b.addSquare(new Square(3, 3, "property", "shut"));
            b.addSquare(new Square(14, 8, "noun", "baba"));
            b.addSquare(new Square(15, 8, "operator", "is"));
            b.addSquare(new Square(16, 8, "property", "you"));
            b.addSquare(new Square(15, 6, "noun", "key"));
            b.addSquare(new Square(15, 5, "operator", "is"));
            b.addSquare(new Square(19, 3, "property", "open"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 22) {
            Board b = new Board(28, 16, 50);
            b.addSquare(new Square(2, 13, "item", "water"));
            for (int i = 11; i <= 14; i++)
                b.addSquare(new Square(3, i, "item", "water"));
            for (int i = 12; i <= 13; i++)
                b.addSquare(new Square(4, i, "item", "water"));
            b.addSquare(new Square(17, 6, "item", "water"));
            for (int i = 17; i <= 20; i++)
                b.addSquare(new Square(i, 5, "item", "water"));
            for (int i = 17; i <= 20; i++)
                b.addSquare(new Square(i, 4, "item", "water"));
            for (int i = 17; i <= 24; i++)
                b.addSquare(new Square(i, 3, "item", "water"));
            for (int i = 17; i <= 22; i++)
                b.addSquare(new Square(i, 2, "item", "water"));
            for (int i = 17; i <= 27; i++)
                b.addSquare(new Square(i, 1, "item", "water"));
            b.addSquare(new Square(4, 16, "item", "water"));
            for (int i = 15; i <= 16; i++)
                b.addSquare(new Square(5, i, "item", "water"));
            for (int i = 14; i <= 16; i++)
                b.addSquare(new Square(6, i, "item", "water"));
            for (int i = 12; i <= 16; i++)
                b.addSquare(new Square(7, i, "item", "water"));
            for (int i = 10; i <= 13; i++)
                b.addSquare(new Square(8, i, "item", "water"));
            for (int i = 8; i <= 11; i++)
                b.addSquare(new Square(9, i, "item", "water"));
            for (int i = 4; i <= 10; i++)
                b.addSquare(new Square(10, i, "item", "water"));
            for (int i = 3; i <= 6; i++)
                b.addSquare(new Square(9, i, "item", "water"));
            for (int i = 1; i <= 4; i++)
                b.addSquare(new Square(8, i, "item", "water"));
            for (int i = 1; i <= 3; i++)
                b.addSquare(new Square(7, i, "item", "water"));
            for (int i = 1; i <= 2; i++)
                b.addSquare(new Square(6, i, "item", "water"));
            b.addSquare(new Square(5, 1, "item", "water"));
            for (int i = 22; i <= 28; i++)
                if (i != 27)
                    b.addSquare(new Square(i, 16, "item", "water"));
            for (int i = 23; i <= 25; i++)
                b.addSquare(new Square(i, 15, "item", "water"));
            for (int i = 23; i <= 24; i++)
                b.addSquare(new Square(i, 14, "item", "water"));
            for (int i = 20; i <= 23; i++)
                b.addSquare(new Square(i, 13, "item", "water"));
            for (int i = 20; i <= 22; i++)
                b.addSquare(new Square(i, 12, "item", "water"));
            for (int i = 19; i <= 21; i++)
                b.addSquare(new Square(i, 11, "item", "water"));
            for (int i = 18; i <= 20; i++)
                b.addSquare(new Square(i, 10, "item", "water"));
            for (int i = 14; i <= 20; i++)
                b.addSquare(new Square(i, 9, "item", "water"));
            for (int i = 4; i <= 10; i++)
                b.addSquare(new Square(13, i, "item", "water"));
            for (int i = 7; i <= 8; i++)
                b.addSquare(new Square(20, i, "item", "water"));
            b.addSquare(new Square(14, 8, "item", "water"));
            b.addSquare(new Square(26, 13, "item", "water"));
            b.addSquare(new Square(20, 9, "item", "water"));
            for (int i = 25; i <= 26; i++)
                b.addSquare(new Square(i, 11, "item", "water"));
            for (int i = 24; i <= 26; i++)
                b.addSquare(new Square(i, 10, "item", "water"));
            b.addSquare(new Square(12, 10, "item", "skull"));
            b.addSquare(new Square(11, 10, "item", "skull"));
            b.addSquare(new Square(12, 4, "item", "skull"));
            b.addSquare(new Square(11, 4, "item", "skull"));
            b.addSquare(new Square(12, 12, "item", "baba"));
            b.addSquare(new Square(20, 14, "item", "jelly"));
            b.addSquare(new Square(20, 6, "item", "door"));
            b.addSquare(new Square(24, 4, "item", "flag"));
            b.addSquare(new Square(23, 2, "noun", "flag"));
            b.addSquare(new Square(24, 2, "operator", "is"));
            b.addSquare(new Square(25, 2, "property", "win"));
            b.addSquare(new Square(1, 16, "noun", "water"));
            b.addSquare(new Square(2, 16, "operator", "is"));
            b.addSquare(new Square(3, 16, "property", "stop"));
            b.addSquare(new Square(7, 8, "noun", "skull"));
            b.addSquare(new Square(7, 7, "operator", "is"));
            b.addSquare(new Square(7, 6, "property", "defeat"));
            b.addSquare(new Square(11, 14, "noun", "baba"));
            b.addSquare(new Square(12, 14, "operator", "is"));
            b.addSquare(new Square(13, 14, "property", "you"));
            b.addSquare(new Square(21, 9, "noun", "door"));
            b.addSquare(new Square(21, 8, "operator", "is"));
            b.addSquare(new Square(21, 7, "property", "sink"));
            b.addSquare(new Square(17, 15, "noun", "jelly"));
            b.addSquare(new Square(17, 14, "operator", "is"));
            b.addSquare(new Square(17, 13, "property", "fall"));
            b.addSquare(new Square(17, 12, "operator", "and"));
            b.addSquare(new Square(17, 11, "property", "push"));
            b.addSquare(new Square(15, 14, "operator", "and"));
            b.update();
            b.checkValid();
            return b;
        } else if (level == 23) {
            Board b = new Board(27, 18, 48);
            b.addSquare(new Square(8, 7, "item", "flag"));
            b.addSquare(new Square(11, 7, "item", "door"));
            b.addSquare(new Square(11, 8, "item", "door"));
            b.addSquare(new Square(17, 7, "item", "door"));
            b.addSquare(new Square(17, 8, "item", "door"));
            b.addSquare(new Square(8, 11, "noun", "baba"));
            b.addSquare(new Square(8, 13, "noun", "key"));
            b.addSquare(new Square(9, 12, "operator", "is"));
            b.addSquare(new Square(11, 12, "operator", "is"));
            b.addSquare(new Square(12, 12, "noun", "key"));
            b.addSquare(new Square(13, 12, "operator", "is"));
            b.addSquare(new Square(14, 12, "property", "you"));
            b.addSquare(new Square(13, 15, "item", "key"));
            b.addSquare(new Square(16, 14, "operator", "is"));
            b.addSquare(new Square(16, 15, "noun", "baba"));
            for (int i = 12; i <= 16; i++)
                b.addSquare(new Square(i, 6, "item", "lava"));
            b.addSquare(new Square(14, 3, "property", "win"));
            b.addSquare(new Square(20, 4, "noun", "key"));
            b.addSquare(new Square(21, 4, "operator", "is"));
            b.addSquare(new Square(22, 4, "property", "open"));
            b.addSquare(new Square(20, 6, "noun", "door"));
            b.addSquare(new Square(21, 6, "operator", "is"));
            b.addSquare(new Square(22, 6, "property", "shut"));
            b.addSquare(new Square(20, 8, "noun", "lava"));
            b.addSquare(new Square(21, 8, "operator", "is"));
            b.addSquare(new Square(22, 8, "property", "hot"));
            b.addSquare(new Square(20, 10, "noun", "baba"));
            b.addSquare(new Square(21, 10, "operator", "is"));
            b.addSquare(new Square(22, 10, "property", "melt"));
            b.addSquare(new Square(20, 12, "noun", "wall"));
            b.addSquare(new Square(21, 12, "operator", "is"));
            b.addSquare(new Square(22, 12, "property", "stop"));
            for (int i = 2; i <= 17; i++)
                b.addSquare(new Square(6, i, "item", "wall"));
            for (int i = 7; i <= 17; i++)
                b.addSquare(new Square(i, 9, "item", "wall"));
            for (int i = 3; i <= 6; i++)
                b.addSquare(new Square(11, i, "item", "wall"));
            for (int i = 7; i <= 17; i++)
                b.addSquare(new Square(i, 2, "item", "wall"));
            for (int i = 3; i <= 6; i++)
                b.addSquare(new Square(17, i, "item", "wall"));
            for (int i = 6; i <= 11; i++)
                b.addSquare(new Square(19, i, "item", "wall"));
            b.addSquare(new Square(18, 6, "item", "wall"));
            b.addSquare(new Square(18, 11, "item", "wall"));
            for (int i = 11; i <= 17; i++)
                b.addSquare(new Square(17, i, "item", "wall"));
            for (int i = 7; i <= 16; i++)
                b.addSquare(new Square(i, 17, "item", "wall"));
            b.addSquare(new Square(8, 5, "noun", "flag"));
            b.addSquare(new Square(9, 5, "operator", "is"));
            b.update();
            b.checkValid();
            return b;
        } else if(level==24){
            Board b = new Board(27, 18, 48);
            b.update();
            b.checkValid();
            return b;
        }
        else {
            Board b = new Board(27, 18, 48);
            b.update();
            b.checkValid();
            return b;
        }
    }
}
