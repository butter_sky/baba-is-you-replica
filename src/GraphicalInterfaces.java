import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * 绘制图形界面
 */
public class GraphicalInterfaces {

    private final JFrame frame = new JFrame("BABA IS YOU");

    /* 用于直接调用图片 */
    Picture picture = new Picture();
    /* 用于直接调用关卡 */
    ArrayList<Board> boards = new ArrayList<>();

    /* 图形界面的大小 */
    final int mapW = 1600;
    final int mapH = 900;

    /* 页面标记 */
    int pageFlag = 0;
    /* 主页面上的选择标记 */
    int titleFlag = 0;
    /* 关卡标记 */
    int chooseFlag = 0;
    /* 设置界面里是否播放的标记 */
    int musicFlag = 0;
    /* 设置界面里的选择标记 */
    int selectFlag = 0;
    /* 设置界面里正在播放的标记 */
    int playFlag = 0;

    private static GraphicalInterfaces instance;

    private GraphicalInterfaces() {
    }

    /**
     * 单例模式
     *
     * @return 返回唯一实例
     */
    public static GraphicalInterfaces getInstance() {
        if (instance == null) {
            instance = new GraphicalInterfaces();
        }
        return instance;
    }

    /**
     * 在某个关卡界面中查找某个块的上下左右是否有相同的块，每种情况对应一种该块的显示，用于设置相同块的相连显示
     *
     * @param board  关卡界面
     * @param square 块
     * @return 该块的图形数组下标
     */
    public int checkImg(Board board, Square square) {
        String a = "";
        if (square.getY() != board.getHeight()
                && board.checkItemSquareByName(square.getX(), square.getY() + 1, square.getName()))
            a = a + "1";
        else if (square.getY() == board.getHeight())
            a = a + "1";
        else a = a + "0";
        if (square.getX() != 1
                && board.checkItemSquareByName(square.getX() - 1, square.getY(), square.getName()))
            a = a + "1";
        else if (square.getX() == 1)
            a = a + "1";
        else a = a + "0";
        if (square.getX() != board.getWidth()
                && board.checkItemSquareByName(square.getX() + 1, square.getY(), square.getName()))
            a = a + "1";
        else if (square.getX() == board.getWidth())
            a = a + "1";
        else a = a + "0";
        if (square.getY() != 1
                && board.checkItemSquareByName(square.getX(), square.getY() - 1, square.getName()))
            a = a + "1";
        else if (square.getY() == 1)
            a = a + "1";
        else a = a + "0";
        return Integer.parseInt(a, 2);
    }

    /**
     * 继承JPanel，重写paint方法，用于绘制图形界面
     */
    private class Draw extends JPanel {
        /**
         * 绘制图形界面
         *
         * @param g 提供基本绘图方法
         */
        @Override
        public void paint(Graphics g) {
            if (pageFlag == 0) {
                g.drawImage(picture.map, 0, 0, mapW, mapH, null);
                g.drawImage(picture.title, 350, 100, 900, 300, null);
                g.drawImage(picture.choose, 562, 500, 476, 265, null);
                g.drawImage(picture.babaRight, 512, 506 + titleFlag * 66, 50, 50, null);
            } else if (pageFlag == 1) {
                g.drawImage(picture.map, 0, 0, mapW, mapH, null);
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 10; j++)
                        g.drawImage(picture.num[i * 10 + j], 300 + 100 * j, 150 + 100 * i, 100, 100, null);
                g.drawImage(picture.selected, 300 + 100 * (chooseFlag % 10), 150 + 100 * (chooseFlag / 10), 100, 100,
                        null);
            } else if (pageFlag == 2) {
                g.drawImage(picture.map, 0, 0, mapW, mapH, null);
                g.drawImage(picture.level[chooseFlag>23?23:chooseFlag], 425, 250, 750, 100, null);
            } else if (pageFlag == 3 || pageFlag == 4) {
                g.drawImage(picture.background, 0, 0, mapW, mapH, null);
                g.drawImage(picture.map,
                        (mapW - boards.get(chooseFlag).getWidth() * boards.get(chooseFlag).getSize()) / 2,
                        (mapH - boards.get(chooseFlag).getHeight() * boards.get(chooseFlag).getSize()) / 2,
                        boards.get(chooseFlag).getWidth() * boards.get(chooseFlag).getSize(),
                        boards.get(chooseFlag).getHeight() * boards.get(chooseFlag).getSize(), null);
                for (int rank = 15; rank >= 1; rank--) {
                    for (Square s : boards.get(chooseFlag).getSquares()) {
                        if (!s.checkPriority(rank))
                            continue;
                        int x = (mapW - boards.get(chooseFlag).getWidth() * boards.get(chooseFlag).getSize()) / 2
                                + (s.getX() - 1) * boards.get(chooseFlag).getSize();
                        int y = (mapH - boards.get(chooseFlag).getHeight() * boards.get(chooseFlag).getSize()) / 2
                                + (boards.get(chooseFlag).getHeight() - s.getY()) * boards.get(chooseFlag).getSize();
                        if (s.getType().equals("item")) {
                            if (s.getName().equals("crab"))
                                g.drawImage(picture.crab, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("door"))
                                g.drawImage(picture.door, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("flag"))
                                g.drawImage(picture.flag, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("grass"))
                                g.drawImage(picture.grass, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("jelly"))
                                g.drawImage(picture.jelly, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("key"))
                                g.drawImage(picture.key, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("lava"))
                                g.drawImage(picture.lava[checkImg(boards.get(chooseFlag), s)], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("pillar"))
                                g.drawImage(picture.pillar, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("rock"))
                                g.drawImage(picture.rock, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("skull"))
                                g.drawImage(picture.skull, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("star"))
                                g.drawImage(picture.star, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("wall"))
                                g.drawImage(picture.wall[checkImg(boards.get(chooseFlag), s)], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("water"))
                                g.drawImage(picture.water[checkImg(boards.get(chooseFlag), s)], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("baba")) {
                                if (s.getDirection() == KeyEvent.VK_UP)
                                    g.drawImage(picture.babaUp, x, y, boards.get(chooseFlag).getSize(),
                                            boards.get(chooseFlag).getSize(), null);
                                else if (s.getDirection() == KeyEvent.VK_RIGHT)
                                    g.drawImage(picture.babaRight, x, y, boards.get(chooseFlag).getSize(),
                                            boards.get(chooseFlag).getSize(), null);
                                else if (s.getDirection() == KeyEvent.VK_DOWN)
                                    g.drawImage(picture.babaDown, x, y, boards.get(chooseFlag).getSize(),
                                            boards.get(chooseFlag).getSize(), null);
                                else
                                    g.drawImage(picture.babaLeft, x, y, boards.get(chooseFlag).getSize(),
                                            boards.get(chooseFlag).getSize(), null);
                            }
                        } else {
                            int a = s.isActive() ? 1 : 0;
                            if (s.getName().equals("and"))
                                g.drawImage(picture.AND[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("baba"))
                                g.drawImage(picture.BABA[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("crab"))
                                g.drawImage(picture.CRAB[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("defeat"))
                                g.drawImage(picture.DEFEAT[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("door"))
                                g.drawImage(picture.DOOR[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("fall"))
                                g.drawImage(picture.FALL[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("flag"))
                                g.drawImage(picture.FLAG[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("grass"))
                                g.drawImage(picture.GRASS[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("hot"))
                                g.drawImage(picture.HOT[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("is"))
                                g.drawImage(picture.IS[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("jelly"))
                                g.drawImage(picture.JELLY[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("key"))
                                g.drawImage(picture.KEY[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("lava"))
                                g.drawImage(picture.LAVA[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("melt"))
                                g.drawImage(picture.MELT[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("open"))
                                g.drawImage(picture.OPEN[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("pillar"))
                                g.drawImage(picture.PILLAR[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("pull"))
                                g.drawImage(picture.PULL[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("push"))
                                g.drawImage(picture.PUSH[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("rock"))
                                g.drawImage(picture.ROCK[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("shut"))
                                g.drawImage(picture.SHUT[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("sink"))
                                g.drawImage(picture.SINK[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("skull"))
                                g.drawImage(picture.SKULL[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("star"))
                                g.drawImage(picture.STAR[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("stop"))
                                g.drawImage(picture.STOP[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("wall"))
                                g.drawImage(picture.WALL[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("water"))
                                g.drawImage(picture.WATER[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("win"))
                                g.drawImage(picture.WIN[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            else if (s.getName().equals("you"))
                                g.drawImage(picture.YOU[a], x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                            if (s.isActive() && !s.isValid())
                                g.drawImage(picture.invalid, x, y, boards.get(chooseFlag).getSize(),
                                        boards.get(chooseFlag).getSize(), null);
                        }
                    }
                }
                if (boards.get(chooseFlag).lose())
                    g.drawImage(picture.rz, 570, 830, 560, 70, null);
                if (chooseFlag == 23)
                    g.drawImage(picture.space, 900, 150, 560, 70, null);
                if (pageFlag == 4)
                    g.drawImage(picture.congratulations, 50, 250, 1500, 150, null);
            } else if (pageFlag == 5) {
                g.drawImage(picture.map, 0, 0, mapW, mapH, null);
                g.drawImage(picture.music[selectFlag][musicFlag], 200, 250, 400, 400, null);
                g.drawImage(picture.selectMusic[selectFlag], 800, 150, 400, 600, null);
                g.drawImage(picture.babaLeft, 1200, 200 + 200 * playFlag, 100, 100, null);
            } else if (pageFlag == 6) {
                g.drawImage(picture.map, 0, 0, mapW, mapH, null);
                g.drawImage(picture.how, 0, 0, 1600, 900, null);
            }
        }
    }

    /* Draw的实例，用于绘制图形界面 */
    Draw draw = new Draw();

    /**
     * 设置键盘及窗口监听和页面转换的功能
     *
     * @throws Exception 出现异常时抛出
     */
    public void init() throws Exception {

        picture.upload();
        Music.play("music/0.wav");
        for (int i = 0; i < 40; i++)
            boards.add(Level.getLevel(i));

        KeyListener keyListener = new KeyAdapter() {
            /**
             * 设置键盘监听
             * @param e 提供对应的按键事件
             */
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                if (pageFlag == 0) {
                    if (keyCode == KeyEvent.VK_UP) {
                        if (titleFlag == 0)
                            titleFlag = 3;
                        else
                            titleFlag = titleFlag - 1;
                    } else if (keyCode == KeyEvent.VK_DOWN) {
                        if (titleFlag == 3)
                            titleFlag = 0;
                        else
                            titleFlag = titleFlag + 1;
                    } else if (keyCode == KeyEvent.VK_ENTER) {
                        if (titleFlag == 0)
                            pageFlag = 1;
                        else if (titleFlag == 1)
                            pageFlag = 5;
                        else if (titleFlag == 2)
                            pageFlag = 6;
                        else if (titleFlag == 3) {
                            System.exit(0);
                            frame.dispose();
                        }
                    }
                } else if (pageFlag == 1) {
                    if (keyCode == KeyEvent.VK_UP) {
                        if (chooseFlag >= 10)
                            chooseFlag = chooseFlag - 10;
                    } else if (keyCode == KeyEvent.VK_DOWN) {
                        if (chooseFlag <= 29)
                            chooseFlag = chooseFlag + 10;
                    } else if (keyCode == KeyEvent.VK_RIGHT) {
                        if (chooseFlag % 10 != 9)
                            chooseFlag = chooseFlag + 1;
                    } else if (keyCode == KeyEvent.VK_LEFT) {
                        if (chooseFlag % 10 != 0)
                            chooseFlag = chooseFlag - 1;
                    } else if (keyCode == KeyEvent.VK_ENTER)
                        pageFlag = 2;
                    else if (keyCode == KeyEvent.VK_ESCAPE)
                        pageFlag = 0;
                } else if (pageFlag == 2) {
                    if (keyCode == KeyEvent.VK_ENTER)
                        pageFlag = 3;
                    else if (keyCode == KeyEvent.VK_ESCAPE)
                        pageFlag = 1;
                } else if (pageFlag == 3) {
                    if (keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_RIGHT || keyCode == KeyEvent.VK_UP
                            || keyCode == KeyEvent.VK_DOWN || keyCode == KeyEvent.VK_SPACE) {
                        boards.get(chooseFlag).save();
                        boards.get(chooseFlag).move(keyCode);
                        boards.get(chooseFlag).update();
                        boards.get(chooseFlag).change();
                        boards.get(chooseFlag).checkFall();
                        boards.get(chooseFlag).checkDeath();
                        boards.get(chooseFlag).update();
                        boards.get(chooseFlag).checkValid();
                        if (!boards.get(chooseFlag).isChanged()) {
                            boards.get(chooseFlag).undo();
                        } else
                            boards.get(chooseFlag).setChanged(false);
                    } else if (keyCode == KeyEvent.VK_ESCAPE) {
                        pageFlag = 1;
                        boards.set(chooseFlag, Level.getLevel(chooseFlag));
                    } else if (keyCode == KeyEvent.VK_R) {
                        boards.set(chooseFlag, Level.getLevel(chooseFlag));
                    } else if (keyCode == KeyEvent.VK_Z) {
                        boards.get(chooseFlag).undo();
                    }
                    if (boards.get(chooseFlag).win()) {
                        draw.repaint();
                        pageFlag = 4;
                    }
                } else if (pageFlag == 4) {
                    if (keyCode == KeyEvent.VK_ENTER) {
                        pageFlag = 1;
                        boards.set(chooseFlag, Level.getLevel(chooseFlag));
                    } else if (keyCode == KeyEvent.VK_ESCAPE) {
                        pageFlag = 1;
                        boards.set(chooseFlag, Level.getLevel(chooseFlag));
                    }
                } else if (pageFlag == 5) {
                    if (keyCode == KeyEvent.VK_ESCAPE)
                        pageFlag = 0;
                    else if (keyCode == KeyEvent.VK_ENTER) {
                        if (selectFlag == 0) {
                            if (musicFlag == 0) {
                                musicFlag = 1;
                                Music.stop();
                            } else {
                                musicFlag = 0;
                                Music.stop();
                                Music.play("music/" + playFlag + ".wav");
                            }
                        } else {
                            musicFlag = 0;
                            playFlag = selectFlag - 1;
                            Music.stop();
                            Music.play("music/" + playFlag + ".wav");
                        }
                    } else if (keyCode == KeyEvent.VK_RIGHT || keyCode == KeyEvent.VK_LEFT) {
                        if (selectFlag == 0)
                            selectFlag = 1;
                        else
                            selectFlag = 0;
                    } else if (keyCode == KeyEvent.VK_UP) {
                        if (selectFlag == 1)
                            selectFlag = 3;
                        else if (selectFlag == 2 || selectFlag == 3)
                            selectFlag--;
                    } else if (keyCode == KeyEvent.VK_DOWN) {
                        if (selectFlag == 3)
                            selectFlag = 1;
                        else if (selectFlag == 1 || selectFlag == 2)
                            selectFlag++;
                    }
                } else if (pageFlag == 6) {
                    if (keyCode == KeyEvent.VK_ENTER)
                        pageFlag = 0;
                    else if (keyCode == KeyEvent.VK_ESCAPE)
                        pageFlag = 0;
                }
                draw.repaint();
            }
        };

        draw.setPreferredSize(new Dimension(mapW, mapH));
        draw.addKeyListener(keyListener);

        frame.add(draw);
        frame.addKeyListener(keyListener);

        frame.addWindowListener(new WindowAdapter() {
            /**
             * 设置窗口监听
             * @param e 提供对应的窗口事件
             */
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        frame.pack();

        frame.setVisible(true);
        frame.setResizable(false);

    }

}
