import java.awt.event.KeyEvent;

/**
 * 游戏中的块
 */
public class Square {
    /* 横坐标 */
    private int x;
    /* 纵坐标 */
    private int y;
    /* 类型（分为item、noun、operator和property） */
    private String type;
    /* 名字（具体类型） */
    private String name;
    /* 朝向 */
    private int direction;
    /* 是否被激活（对于文本块） */
    private boolean isActive;
    /* 是否有效（对于文本块） */
    private boolean isValid;

    public Square() {

    }

    public Square(int x, int y, String type, String name) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.name = name;
        this.direction = KeyEvent.VK_RIGHT;
        this.isActive = false;
        this.isValid = false;
    }

    public Square(int x, int y, String type, String name, int direction) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.name = name;
        this.direction = direction;
        this.isActive = false;
        this.isValid = false;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean isValid) {
        this.isValid = isValid;
    }

    public Square clone() {
        Square clone = new Square();
        clone.x = this.x;
        clone.y = this.y;
        clone.type = this.type;
        clone.name = this.name;
        clone.direction = this.direction;
        clone.isActive = this.isActive;
        clone.isValid = this.isValid;
        return clone;
    }

    /**
     * 判断块是否为某一显示优先级
     *
     * @param rank 显示优先级
     * @return 返回真假
     */
    public boolean checkPriority(int rank) {
        if (!type.equals("item")) {
            return rank == 1;
        } else if (name.equals("baba")) {
            return rank == 2;
        } else if (name.equals("crab")) {
            return rank == 3;
        } else if (name.equals("jelly")) {
            return rank == 4;
        } else if (name.equals("flag")) {
            return rank == 5;
        } else if (name.equals("key")) {
            return rank == 6;
        } else if (name.equals("star")) {
            return rank == 7;
        } else if (name.equals("rock")) {
            return rank == 8;
        } else if (name.equals("skull")) {
            return rank == 9;
        } else if (name.equals("pillar")) {
            return rank == 10;
        } else if (name.equals("door")) {
            return rank == 11;
        } else if (name.equals("grass")) {
            return rank == 12;
        } else if (name.equals("wall")) {
            return rank == 13;
        } else if (name.equals("water")) {
            return rank == 14;
        } else if (name.equals("lava")) {
            return rank == 15;
        } else return false;
    }
}