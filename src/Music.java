import java.applet.Applet;
import java.applet.AudioClip;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

/**
 * 编写对音频操作的方法
 */
public class Music {

    private static AudioClip audioClip;

    /**
     * 播放音频
     *
     * @param name 音频文件的相对地址
     */
    public static void play(String name) {
        URL file = Objects.requireNonNull(Picture.class.getResource(name));
        try {
            audioClip = Applet.newAudioClip(file.toURI().toURL());
        } catch (MalformedURLException | URISyntaxException e) {
            e.printStackTrace();
        }
        audioClip.loop();
    }

    /**
     * 停止播放音频
     */
    public static void stop() {
        audioClip.stop();
    }

}
