import java.util.HashMap;
import java.util.HashSet;

/**
 * 游戏中某一名字的块对应的状态
 */
public class State {
    private HashMap<String, Object> properties;
    /* 要变成的块 */
    private HashSet<String> changes = new HashSet<>();

    public HashMap<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(HashMap<String, Object> properties) {
        this.properties = properties;
    }

    public HashSet<String> getChanges() {
        return changes;
    }

    public void setChanges(HashSet<String> changes) {
        this.changes = changes;
    }

    /**
     * 将状态重置
     */
    public void reset() {
        properties = new HashMap<>();
        changes = new HashSet<>();
    }

    /**
     * 将某一状态置为真
     *
     * @param name 块的名字
     */
    public void addProperty(String name) {
        if (!properties.containsKey(name)) {
            properties.put(name, new Object());
            if (name.equals("push") || name.equals("pull"))
                addProperty("stop");
        }
    }

    public boolean hasProperty(String name) {
        return properties.containsKey(name);
    }

    public State clone() {
        State clone = new State();
        for (String key : properties.keySet()) {
            clone.properties.put(key, properties.get(key));
        }
        clone.changes.addAll(this.changes);
        return clone;
    }
}
