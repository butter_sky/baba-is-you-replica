import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

/**
 * 声明所有图片并编写方法进行全部读入
 */
public class Picture {

    BufferedImage background;
    BufferedImage map;

    // title
    BufferedImage title;
    BufferedImage choose;

    // choose
    BufferedImage[] num = new BufferedImage[40];
    BufferedImage selected;

    // subtitle
    BufferedImage[] level = new BufferedImage[24];

    // map
    BufferedImage[] AND = new BufferedImage[2];
    BufferedImage[] BABA = new BufferedImage[2];
    BufferedImage[] CRAB = new BufferedImage[2];
    BufferedImage[] DEFEAT = new BufferedImage[2];
    BufferedImage[] DOOR = new BufferedImage[2];
    BufferedImage[] FALL = new BufferedImage[2];
    BufferedImage[] FLAG = new BufferedImage[2];
    BufferedImage[] GRASS = new BufferedImage[2];
    BufferedImage[] HOT = new BufferedImage[2];
    BufferedImage[] IS = new BufferedImage[2];
    BufferedImage[] JELLY = new BufferedImage[2];
    BufferedImage[] KEY = new BufferedImage[2];
    BufferedImage[] LAVA = new BufferedImage[2];
    BufferedImage[] MELT = new BufferedImage[2];
    BufferedImage[] OPEN = new BufferedImage[2];
    BufferedImage[] PILLAR = new BufferedImage[2];
    BufferedImage[] PULL = new BufferedImage[2];
    BufferedImage[] PUSH = new BufferedImage[2];
    BufferedImage[] ROCK = new BufferedImage[2];
    BufferedImage[] SHUT = new BufferedImage[2];
    BufferedImage[] SINK = new BufferedImage[2];
    BufferedImage[] SKULL = new BufferedImage[2];
    BufferedImage[] STAR = new BufferedImage[2];
    BufferedImage[] STOP = new BufferedImage[2];
    BufferedImage[] WALL = new BufferedImage[2];
    BufferedImage[] WATER = new BufferedImage[2];
    BufferedImage[] WIN = new BufferedImage[2];
    BufferedImage[] YOU = new BufferedImage[2];

    BufferedImage babaDown;
    BufferedImage babaLeft;
    BufferedImage babaRight;
    BufferedImage babaUp;
    BufferedImage crab;
    BufferedImage door;
    BufferedImage flag;
    BufferedImage grass;
    BufferedImage jelly;
    BufferedImage key;
    BufferedImage pillar;
    BufferedImage rock;
    BufferedImage skull;
    BufferedImage star;

    BufferedImage[] lava = new BufferedImage[16];
    BufferedImage[] water = new BufferedImage[16];
    BufferedImage[] wall = new BufferedImage[16];

    BufferedImage invalid;

    // congratulations
    BufferedImage congratulations;

    // settings
    BufferedImage[][] music = new BufferedImage[4][2];
    BufferedImage[] selectMusic = new BufferedImage[4];

    // how
    BufferedImage how;
    BufferedImage rz;
    BufferedImage space;

    /**
     * upload 对所有图片进行读入
     *
     * @throws IOException 无法读取相应文件则抛出
     */
    public void upload() throws IOException {

        background = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/background.png")));
        map = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/map.png")));

        // title
        title = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/title.png")));
        choose = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/choose.png")));

        // choose
        for (int i = 0; i < 40; i++)
            num[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/number/" + i + ".png")));
        selected = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/number/selected.png")));

        // subtitle
        for (int i = 0; i < 24; i++)
            level[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/subtitle/" + i + ".png")));

        // congratulations
        congratulations = ImageIO
                .read(Objects.requireNonNull(Picture.class.getResource("img/game/congratulations.png")));

        // settings
        for (int i = 0; i < 2; i++)
            music[0][i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/m" + i + ".png")));
        for (int i = 0; i < 2; i++)
            music[1][i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/music" + i + ".png")));
        for (int i = 0; i < 2; i++)
            music[2][i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/music" + i + ".png")));
        for (int i = 0; i < 2; i++)
            music[3][i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/music" + i + ".png")));
        for (int i = 0; i < 4; i++)
            selectMusic[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/s" + i + ".png")));

        // how
        how = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/how.png")));
        rz = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/RZ.png")));
        space = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/space.png")));

        // map
        for (int i = 0; i < 2; i++)
            AND[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/and" + i + ".png")));
        for (int i = 0; i < 2; i++)
            BABA[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/baba" + i + ".png")));
        for (int i = 0; i < 2; i++)
            CRAB[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/crab" + i + ".png")));
        for (int i = 0; i < 2; i++)
            DEFEAT[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/defeat" + i + ".png")));
        for (int i = 0; i < 2; i++)
            DOOR[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/door" + i + ".png")));
        for (int i = 0; i < 2; i++)
            FALL[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/fall" + i + ".png")));
        for (int i = 0; i < 2; i++)
            FLAG[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/flag" + i + ".png")));
        for (int i = 0; i < 2; i++)
            GRASS[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/grass" + i + ".png")));
        for (int i = 0; i < 2; i++)
            HOT[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/hot" + i + ".png")));
        for (int i = 0; i < 2; i++)
            IS[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/is" + i + ".png")));
        for (int i = 0; i < 2; i++)
            JELLY[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/jelly" + i + ".png")));
        for (int i = 0; i < 2; i++)
            KEY[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/key" + i + ".png")));
        for (int i = 0; i < 2; i++)
            LAVA[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/lava" + i + ".png")));
        for (int i = 0; i < 2; i++)
            MELT[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/melt" + i + ".png")));
        for (int i = 0; i < 2; i++)
            OPEN[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/open" + i + ".png")));
        for (int i = 0; i < 2; i++)
            PILLAR[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/pillar" + i + ".png")));
        for (int i = 0; i < 2; i++)
            PULL[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/pull" + i + ".png")));
        for (int i = 0; i < 2; i++)
            PUSH[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/push" + i + ".png")));
        for (int i = 0; i < 2; i++)
            ROCK[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/rock" + i + ".png")));
        for (int i = 0; i < 2; i++)
            SHUT[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/shut" + i + ".png")));
        for (int i = 0; i < 2; i++)
            SINK[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/sink" + i + ".png")));
        for (int i = 0; i < 2; i++)
            SKULL[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/skull" + i + ".png")));
        for (int i = 0; i < 2; i++)
            STAR[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/star" + i + ".png")));
        for (int i = 0; i < 2; i++)
            STOP[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/stop" + i + ".png")));
        for (int i = 0; i < 2; i++)
            WALL[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/wall" + i + ".png")));
        for (int i = 0; i < 2; i++)
            WATER[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/water" + i + ".png")));
        for (int i = 0; i < 2; i++)
            WIN[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/win" + i + ".png")));
        for (int i = 0; i < 2; i++)
            YOU[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/text/you" + i + ".png")));

        babaDown = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/baba_down.png")));
        babaLeft = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/baba_left.png")));
        babaRight = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/baba_right.png")));
        babaUp = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/baba_up.png")));
        crab = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/crab.png")));
        door = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/door.png")));
        flag = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/flag.png")));
        grass = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/grass.png")));
        jelly = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/jelly.png")));
        key = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/key.png")));
        pillar = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/pillar.png")));
        rock = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/rock.png")));
        skull = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/skull.png")));
        star = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/star.png")));

        for (int i = 0; i < 16; i++)
            lava[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/lava/" + i + ".png")));
        for (int i = 0; i < 16; i++)
            water[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/water/" + i + ".png")));
        for (int i = 0; i < 16; i++)
            wall[i] = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/object/wall/" + i + ".png")));

        invalid = ImageIO.read(Objects.requireNonNull(Picture.class.getResource("img/game/invalid.png")));
    }

}
