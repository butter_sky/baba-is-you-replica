import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * 游戏中某一关对应的面板
 */
public class Board {
    /* 宽 */
    private int width;
    /* 高 */
    private int height;
    /* 块的大小 */
    private int size;
    /* 所有块 */
    private ArrayList<Square> squares;
    /* 所有名字的块的状态 */
    private HashMap<String, State> states;
    /* 所有块的历史记录 */
    private Stack<ArrayList<Square>> stackSquares = new Stack<>();
    /* 所有名字的块的状态的历史记录 */
    private Stack<HashMap<String, State>> stackStates = new Stack<>();
    /* 本次操作是否发生状态改变 */
    private boolean isChanged;

    public Board(int width, int height, int size, ArrayList<Square> squares) {
        this.width = width;
        this.height = height;
        this.size = size;
        this.squares = squares;
        for (Square square : squares) {
            if (square.getType().equals("item") && states.get(square.getName()) == null) {
                states.put(square.getName(), new State());
            }
        }
    }

    public Board(int width, int height, int size) {
        this.width = width;
        this.height = height;
        this.size = size;
        this.squares = new ArrayList<>();
        this.states = new HashMap<>();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public ArrayList<Square> getSquares() {
        return squares;
    }

    public void setSquares(ArrayList<Square> squares) {
        this.squares = squares;
    }

    public HashMap<String, State> getStates() {
        return states;
    }

    public void setStates(HashMap<String, State> states) {
        this.states = states;
    }

    public Stack<ArrayList<Square>> getStackSquares() {
        return stackSquares;
    }

    public void setStackSquares(Stack<ArrayList<Square>> stackSquares) {
        this.stackSquares = stackSquares;
    }

    public Stack<HashMap<String, State>> getStackStates() {
        return stackStates;
    }

    public void setStackStates(Stack<HashMap<String, State>> stackStates) {
        this.stackStates = stackStates;
    }

    public boolean isChanged() {
        return isChanged;
    }

    public void setChanged(boolean isChanged) {
        this.isChanged = isChanged;
    }

    /**
     * 初始化时加入一个块
     *
     * @param square 块
     */
    public void addSquare(Square square) {
        squares.add(square);
        if ((square.getType().equals("item") || square.getType().equals("noun"))
                && states.get(square.getName()) == null) {
            states.put(square.getName(), new State());
        }
    }

    /**
     * 得到某一位置的不是"item"的块
     *
     * @param x 横坐标
     * @param y 纵坐标
     * @return 块或null
     */
    public Square getSquareNotItem(int x, int y) {
        for (Square square : squares) {
            if (square.getX() == x && square.getY() == y && !square.getType().equals("item"))
                return square;
        }
        return null;
    }

    /**
     * 查看某位置是否有某名字的类型为“item”的块
     *
     * @param x    横坐标
     * @param y    纵坐标
     * @param name 名字
     * @return boolean
     */
    public boolean checkItemSquareByName(int x, int y, String name) {
        for (Square square : squares) {
            if (square.getX() == x && square.getY() == y && square.getType().equals("item")
                    && square.getName().equals(name))
                return true;
        }
        return false;
    }

    /**
     * 把块变成要变的块
     */
    public void change() {
        for (int i = squares.size() - 1; i >= 0; i--) {
            Square square = squares.get(i);
            if (square.getType().equals("item")) {
                HashSet<String> changes = states.get(square.getName()).getChanges();
                if (changes.isEmpty())
                    continue;
                boolean stay = false;
                for (String change : changes) {
                    if (change.equals(square.getName())) {
                        stay = true;
                        break;
                    }
                }
                if (stay)
                    continue;
                isChanged = true;
                for (String change : changes) {
                    squares.add(new Square(square.getX(), square.getY(), "item", change));
                }
                squares.remove(i);
            }
        }
    }

    /**
     * 得到某一"is"块的某个方向的所有主语和宾语
     *
     * @param subjects 主语容器
     * @param objects  宾语容器
     * @param square   "is"块
     * @param dx       方向横坐标
     * @param dy       方向纵坐标
     */
    public void getSubjectsAndObjects(ArrayList<Square> subjects, ArrayList<Square> objects, Square square, int dx, int dy) {
        int nx = square.getX();
        int ny = square.getY();
        while (true) {
            Square subject, operator;
            nx -= dx;
            ny -= dy;
            subject = getSquareNotItem(nx, ny);
            if (subject == null || !subject.getType().equals("noun"))
                break;
            subjects.add(subject);
            nx -= dx;
            ny -= dy;
            operator = getSquareNotItem(nx, ny);
            if (operator == null || !operator.getType().equals("operator") || !operator.getName().equals("and")) {
                break;
            }
        }
        nx = square.getX();
        ny = square.getY();
        while (true) {
            Square object, operator;
            nx += dx;
            ny += dy;
            object = getSquareNotItem(nx, ny);
            if (object == null || object.getType().equals("operator"))
                break;
            objects.add(object);
            nx += dx;
            ny += dy;
            operator = getSquareNotItem(nx, ny);
            if (operator == null || !operator.getType().equals("operator") || !operator.getName().equals("and")) {
                break;
            }
        }
    }

    /**
     * 获取某一"is"块的某个方向的语句的信息并更新
     *
     * @param square "is"块
     * @param dx     方向横坐标
     * @param dy     方向纵坐标
     */
    public void ReadSentence(Square square, int dx, int dy) {
        ArrayList<Square> subjects = new ArrayList<>();
        ArrayList<Square> objects = new ArrayList<>();
        getSubjectsAndObjects(subjects, objects, square, dx, dy);
        for (Square subject : subjects) {
            for (Square object : objects) {
                subject.setActive(true);
                int nx = subject.getX() + dx;
                int ny = subject.getY() + dy;
                getSquareNotItem(nx, ny).setActive(true);
                object.setActive(true);
                nx = object.getX() - dx;
                ny = object.getY() - dy;
                getSquareNotItem(nx, ny).setActive(true);
                square.setActive(true);
                if (object.getType().equals("property")) {
                    State state = states.get(subject.getName());
                    state.addProperty(object.getName());
                } else if (object.getType().equals("noun")) {
                    State state = states.get(subject.getName());
                    state.getChanges().add(object.getName());
                }
            }
        }
    }

    /**
     * 更新各种名字的块的状态
     */
    public void update() {
        for (State state : states.values())
            state.reset();
        for (Square square : squares) {
            square.setActive(false);
        }
        for (Square square : squares) {
            if (square.getType().equals("operator")) {
                if (square.getName().equals("is")) {
                    ReadSentence(square, 1, 0);
                    ReadSentence(square, 0, -1);
                }
            }
        }
    }

    /**
     * 保存上一次操作后的信息
     */
    public void save() {
        ArrayList<Square> newSquares = new ArrayList<>();
        HashMap<String, State> newStates = new HashMap<>();
        for (Square item : squares) {
            newSquares.add(item.clone());
        }
        for (String key : states.keySet()) {
            newStates.put(key, states.get(key));
        }
        stackSquares.push(newSquares);
        stackStates.push(newStates);
    }

    /**
     * 操作所有具有"you"属性的块和能被拉动或推动的块移动
     *
     * @param direction 方向
     */
    public void move(int direction) {
        int dx = 0, dy = 0;
        if (direction == KeyEvent.VK_LEFT) {
            dx = -1;
        } else if (direction == KeyEvent.VK_RIGHT) {
            dx = 1;
        } else if (direction == KeyEvent.VK_UP) {
            dy = 1;
        } else if (direction == KeyEvent.VK_DOWN) {
            dy = -1;
        } else if (direction == KeyEvent.VK_SPACE)
            return;
        int n = squares.size();
        ArrayList<Boolean> vis1 = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            vis1.add(false);
        }
        ArrayList<Boolean> vis2 = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            vis2.add(false);
        }
        Queue<Integer> queue = new LinkedList<>();
        Queue<Integer> queue2 = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            Square square = squares.get(i);
            if (square.getType().equals("item") && states.get(square.getName()).hasProperty("you")) {
                queue.offer(i);
                if (square.getDirection() != direction) {
                    square.setDirection(direction);
                    isChanged = true;
                }
                vis1.set(i, true);
            }
        }
        while (!queue.isEmpty()) {
            int i = queue.poll();
            Square square = squares.get(i);
            int nx = square.getX() + dx, ny = square.getY() + dy;
            boolean stop = false;
            for (int j = 0; j < n; j++) {
                if (vis1.get(j))
                    continue;
                Square nsquare = squares.get(j);
                if (nx == nsquare.getX() && ny == nsquare.getY()) {
                    if (nsquare.getType().equals("item") && states.get(nsquare.getName()).hasProperty("stop")
                            && !states.get(nsquare.getName()).hasProperty("push") && !states.get(nsquare.getName()).hasProperty("you")) {
                        if (states.get(nsquare.getName()).hasProperty("shut") && square.getType().equals("item")
                                && states.get(square.getName()).hasProperty("open"))
                            continue;
                        stop = true;
                    }
                }
            }
            if (stop || nx > width || nx < 1 || ny < 1 || ny > height) {
                queue2.add(i);
                vis2.set(i, true);
                continue;
            }
            for (int j = 0; j < n; j++) {
                Square nsquare = squares.get(j);
                if (vis1.get(j))
                    continue;
                if (nsquare.getX() == square.getX() + dx && nsquare.getY() == square.getY() + dy
                        && (!nsquare.getType().equals("item") || states.get(nsquare.getName()).hasProperty("push"))) {
                    queue.add(j);
                    vis1.set(j, true);
                } else if (nsquare.getX() == square.getX() - dx && nsquare.getY() == square.getY() - dy
                        && nsquare.getType().equals("item") && states.get(nsquare.getName()).hasProperty("pull")) {
                    queue.add(j);
                    vis1.set(j, true);
                }
            }
        }
        while (!queue2.isEmpty()) {
            Square square = squares.get(queue2.poll());
            boolean stop = false;
            for (int i = 0; i < n; i++) {
                Square nsquare = squares.get(i);
                if (vis1.get(i) && !vis2.get(i))
                    continue;
                if (nsquare.getX() == square.getX() && nsquare.getY() == square.getY()) {
                    if (!nsquare.getType().equals("item") || states.get(nsquare.getName()).hasProperty("stop")) {
                        stop = true;
                        break;
                    }
                }
            }
            for (int i = 0; i < n; i++) {
                Square nsquare = squares.get(i);
                if (!vis1.get(i) || vis2.get(i))
                    continue;
                if (nsquare.getX() == square.getX() - dx && nsquare.getY() == square.getY() - dy) {
                    if (stop || nsquare.getType().equals("item") && states.get(nsquare.getName()).hasProperty("pull")) {
                        queue2.add(i);
                        vis2.set(i, true);
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            if (!vis1.get(i) || vis2.get(i))
                continue;
            isChanged = true;
            Square square = squares.get(i);
            square.setX(square.getX() + dx);
            square.setY(square.getY() + dy);
            square.setDirection(direction);
        }
    }

    /**
     * 将所有需要消失的块清除
     */
    public void checkDeath() {
        ArrayList<Boolean> tag = new ArrayList<>();
        for (int i = 0; i < squares.size(); i++)
            tag.add(false);
        for (int i = 0; i < squares.size(); i++) {
            for (int j = 0; j < squares.size(); j++) {
                Square square1 = squares.get(i);
                Square square2 = squares.get(j);
                if (square1.getX() == square2.getX() && square1.getY() == square2.getY()) {
                    if (square1.getType().equals("item") && square2.getType().equals("item")) {
                        if (states.get(square1.getName()).hasProperty("defeat") && states.get(square2.getName()).hasProperty("you")) {
                            tag.set(j, true);
                        }
                    }
                    if (square1.getType().equals("item") && square2.getType().equals("item")) {
                        if (states.get(square1.getName()).hasProperty("shut") && states.get(square2.getName()).hasProperty("open")) {
                            tag.set(i, true);
                            tag.set(j, true);
                        }
                    }
                    if (square1.getType().equals("item") && square2.getType().equals("item")) {
                        if (states.get(square1.getName()).hasProperty("hot") && states.get(square2.getName()).hasProperty("melt")) {
                            tag.set(j, true);
                        }
                    }
                    if (square1.getType().equals("item")) {
                        if (states.get(square1.getName()).hasProperty("sink") && i != j) {
                            tag.set(i, true);
                            tag.set(j, true);
                        }
                    }
                }
            }
        }
        for (int i = squares.size() - 1; i >= 0; i--) {
            if (tag.get(i)) {
                squares.remove(i);
            }
        }
    }

    /**
     * 让需要下落的块下落
     */
    public void checkFall() {
        ArrayList<Integer> moves = new ArrayList<>();
        for (Square square : squares) {
            if (square.getType().equals("item") && states.get(square.getName()).hasProperty("fall")) {
                ArrayList<Boolean> tag1 = new ArrayList<>();
                ArrayList<Boolean> tag2 = new ArrayList<>();
                for (int i = 0; i < square.getY(); i++) {
                    tag1.add(false);
                }
                for (int i = 0; i < square.getY(); i++) {
                    tag2.add(false);
                }
                for (Square nsquare : squares) {
                    if (nsquare.getX() == square.getX() && nsquare.getY() < square.getY()) {
                        if (!nsquare.getType().equals("item") || !states.get(nsquare.getName()).hasProperty("fall") && states.get(nsquare.getName()).hasProperty("stop")) {
                            tag2.set(nsquare.getY(), true);
                        }
                        if (nsquare.getType().equals("item") && states.get(nsquare.getName()).hasProperty("fall") && states.get(nsquare.getName()).hasProperty("stop")) {
                            tag1.set(nsquare.getY(), true);
                        }
                    }
                }
                int cnt = 0;
                for (int i = 1; i < square.getY(); i++) {
                    if (tag2.get(i))
                        cnt = 0;
                    else if (!tag1.get(i)) {
                        cnt++;
                    }
                }
                moves.add(cnt);
            } else moves.add(0);
        }
        for (int i = 0; i < squares.size(); i++) {
            if (moves.get(i) > 0)
                isChanged = true;
            Square square = squares.get(i);
            square.setY(square.getY() - moves.get(i));
        }
    }

    /**
     * 判断某一"is"块的某个方向的语句中每个块是否有效
     *
     * @param square "is"块
     * @param dx     方向横坐标
     * @param dy     方向纵坐标
     */
    public void checkValidSentence(Square square, int dx, int dy) {
        ArrayList<Square> subjects = new ArrayList<>();
        ArrayList<Square> objects = new ArrayList<>();
        getSubjectsAndObjects(subjects, objects, square, dx, dy);
        for (Square subject : subjects) {
            for (Square object : objects) {
                if (object.getType().equals("property")) {
                    subject.setValid(true);
                    object.setValid(true);
                    square.setValid(true);
                } else if (object.getType().equals("noun")) {
                    State state = states.get(subject.getName());
                    boolean stay = false;
                    for (String change : state.getChanges()) {
                        if (change.equals(subject.getName())) {
                            stay = true;
                            break;
                        }
                    }
                    if (!stay || object.getName().equals(subject.getName())) {
                        subject.setValid(true);
                        object.setValid(true);
                        square.setValid(true);
                    }
                }
            }
        }
    }

    /**
     * 判断所有被激活的块是否有效
     */
    public void checkValid() {
        for (Square square : squares) {
            square.setValid(false);
            if (square.getType().equals("operator") && square.getName().equals("and"))
                square.setValid(true);
        }
        for (Square square : squares) {
            if (square.getType().equals("operator")) {
                if (square.getName().equals("is")) {
                    checkValidSentence(square, 1, 0);
                    checkValidSentence(square, 0, -1);
                }
            }
        }
    }

    /**
     * 撤销操作
     */
    public void undo() {
        if (stackSquares.size() != 0) {
            squares = stackSquares.pop();
            states = stackStates.pop();
        }
    }

    /**
     * 判断是否胜利
     *
     * @return 真假
     */
    public boolean win() {
        for (Square square1 : squares) {
            for (Square square2 : squares) {
                if (square1.getX() == square2.getX() && square1.getY() == square2.getY()) {
                    if (square1.getType().equals("item") && square2.getType().equals("item")) {
                        if (states.get(square1.getName()).hasProperty("win") && states.get(square2.getName()).hasProperty("you")) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * 判断是否失败
     *
     * @return 真假
     */
    public boolean lose() {
        for (Square square : squares) {
            if (square.getType().equals("item") && states.get(square.getName()).hasProperty("you")) {
                return false;
            }
        }
        return true;
    }
    /*public void movestage(){
        int n=squares.size();
        ArrayList<Integer> directionchanges = new ArrayList<>();
        for(int i=0;i<n;i++)
            directionchanges.add(0);
        for(int i=0;i<n;i++)
        {
            Square s1=squares.get(i);
            if(s1.getType().equals("item")&&states.get(s1.getName()).isMove()&&directionchanges.get(i)==0){
                int x=s1.getX(),y= s1.getY(), dx=0,dy=0,direction=s1.getDirection();
                boolean stop=false;
                if (direction == KeyEvent.VK_LEFT) {
                    dx = -1;
                } else if (direction == KeyEvent.VK_RIGHT) {
                    dx = 1;
                } else if (direction == KeyEvent.VK_UP) {
                    dy = 1;
                } else if (direction == KeyEvent.VK_DOWN) {
                    dy = -1;
                }
                while(true){
                    x+=dx;
                    y+=dy;
                    if(dx<1||dx>width||dy<1||dy>height)
                    {
                        stop=true;
                        break;
                    }
                    for(Square s2:squares){
                        if(s2.getType().equals("item")&&states.get(s2.getName()).isMove()&&directionchanges.get(i)==0)
                    }
                }
            }
        }
    }*/
}
